#!/usr/bin/env sh

for tool in `ls tools | grep mlir-`; do
	(cd tools/$tool;
		cat CMakeLists.txt | \
		tr '\n' '\r' | \
		sed 's/set(LIB_LIBS.*LIB_LIBS})//g' | \
		sed 's/set(LIBS.*)\radd_llvm_executable/add_llvm_executable/g' | \
		sed "s|add_llvm_executable($tool|add_llvm_tool($tool|g" | \
		sed "s|whole_archive_link($tool.*||g" | \
		tr '\r' '\n' > CMakeLists.txt)
	(cd test;
		cat CMakeLists.txt | \
		sed 's/add_subdirectory(TestDialect)//g' | \
		sed 's/mlir-test-opt//g' > CMakeLists.txt)
done

echo "endif()" >> tools/mlir-cuda-runner/CMakeLists.txt
