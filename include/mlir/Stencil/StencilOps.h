//===- StencilOps.h - Stencil Operations -------------------------*- C++-*-===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#ifndef MLIR_STENCIL_STENCILOPS_H_
#define MLIR_STENCIL_STENCILOPS_H_

#include "mlir/IR/Builders.h"
#include "mlir/IR/OpDefinition.h"
#include "mlir/Stencil/StencilTraits.h"
#include "mlir/Stencil/StencilTypes.h"
#include "mlir/Support/LLVM.h"

namespace mlir {
namespace Stencil {

#define GET_OP_CLASSES
#include "mlir/Stencil/StencilOps.h.inc"

} // namespace Stencil
} // namespace mlir

#endif // MLIR_STENCIL_STENCILOPS_H_
