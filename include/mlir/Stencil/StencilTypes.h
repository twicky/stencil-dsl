//===- StencilTypes.h - Stencil Types -------------------------------------===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#ifndef MLIR_STENCIL_STENCILTYPES_H_
#define MLIR_STENCIL_STENCILTYPES_H_

#include "mlir/IR/Dialect.h"
#include "mlir/IR/Types.h"

namespace mlir {
class MLIRContext;

namespace Stencil {

enum StencilTypes {
  StencilField = Type::FIRST_STENCIL_TYPE,
  StencilOffset,
  LAST_USED_STENCIL_TYPE = StencilOffset,
};

namespace detail {
struct StencilFieldStorage;
}

class StencilDialect : public Dialect {
public:
  explicit StencilDialect(MLIRContext *context);

  /// Parse a type registered to this dialect.
  Type parseType(llvm::StringRef spec, Location loc) const override;

  /// Print a type registered to this dialect.
  void printType(Type type, llvm::raw_ostream &os) const override;

  static StringRef getDialectNamespace() { return "stencil"; }
};

class StencilFieldType : public Type::TypeBase<StencilFieldType, Type,
                                               detail::StencilFieldStorage> {
public:
  using Base::Base;

  static StencilFieldType get(MLIRContext *context, Type elementsType, bool i,
                              bool j, bool k);

  static bool kindof(unsigned kind) {
    return kind == StencilTypes::StencilField;
  }

  Type getElementType() const;
  bool iDirection() const;
  bool jDirection() const;
  bool kDirection() const;
  ArrayRef<bool> getDirections() const;
};

class StencilOffsetType : public Type::TypeBase<StencilOffsetType, Type> {
public:
  using Base::Base;

  static StencilOffsetType get(MLIRContext *context);

  static bool kindof(unsigned kind) {
    return kind == StencilTypes::StencilOffset;
  }
};

} // namespace Stencil
} // namespace mlir

#endif // MLIR_STENCIL_STENCILTYPES_H_
