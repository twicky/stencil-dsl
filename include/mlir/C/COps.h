//===- COps.h - C Operations -------------------------*- C++-*-===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#ifndef MLIR_C_COPS_H_
#define MLIR_C_COPS_H_

#include "mlir/C/CTraits.h"
#include "mlir/C/CTypes.h"
#include "mlir/IR/OpDefinition.h"
#include "mlir/Linalg/IR/LinalgTraits.h"
#include "mlir/Support/LLVM.h"

namespace mlir {

namespace C {

#define GET_OP_CLASSES
#include "mlir/C/COps.h.inc"

class ForOp
    : public Op<ForOp, OpTrait::NOperands<2>::Impl, OpTrait::ZeroResult> {
public:
  using Op::Op;

  // Hooks to customize behavior of this op.
  static void build(Builder *builder, OperationState *result, Value *LowerBound,
                    Value *UpperBound);

  static StringRef getOperationName() { return "C.for"; }

  /// Returns the 'body' region.
  Region &getBody();

  Value *getLowerBound() { return getOperand(0); }
  Value *getUpperBound() { return getOperand(1); }
  Value *getIterator() { return getBody().front().getArgument(0); }

  LogicalResult verify();
  static ParseResult parse(OpAsmParser *parser, OperationState *result);
  void print(OpAsmPrinter *p);
};

} // namespace C

} // namespace mlir

#endif // MLIR_C_COPS_H_
