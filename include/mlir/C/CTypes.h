//===- CTypes.h - C Types -------------------------------------===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#ifndef MLIR_C_CTYPES_H_
#define MLIR_C_CTYPES_H_

#include <string>

#include "mlir/IR/Dialect.h"
#include "mlir/IR/Types.h"

namespace mlir {
class MLIRContext;

class CDialect : public Dialect {
public:
  explicit CDialect(MLIRContext *context);

  /// Parse a type registered to this dialect.
  Type parseType(llvm::StringRef spec, Location loc) const override;

  /// Print a type registered to this dialect.
  void printType(Type type, llvm::raw_ostream &os) const override;

  static StringRef getDialectNamespace() { return "C"; }
};

namespace C {

enum CTypes {
  CVoidPtrTy = Type::FIRST_C_TYPE,
  LAST_USED_C_TYPE = CVoidPtrTy,
};

class VoidPtrType : public Type::TypeBase<VoidPtrType, Type> {
public:
  // Used for generic hooks in TypeBase.
  using Base::Base;
  /// Construction hook.
  static VoidPtrType get(MLIRContext *context) {
    /// Custom, uniq'ed construction in the MLIRContext.
    return Base::get(context, CTypes::CVoidPtrTy);
  }
  /// Used to implement llvm-style cast.
  static bool kindof(unsigned kind) { return kind == CTypes::CVoidPtrTy; }

  static std::string getCRepr() { return "void *"; }
};

} // namespace C
} // namespace mlir

#endif // MLIR_C_CTYPES_H_
