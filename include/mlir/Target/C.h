//===- C.h - C conversion ---------------------------------------*- C++ -*-===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file declares the entry point for the MLIR to C conversion.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_TARGET_C_H
#define MLIR_TARGET_C_H

#include <memory>
#include <string>

#include "mlir/IR/Module.h"

// Forward-declare LLVM classses.
namespace llvm {
class LLVMContext;
class Module;
} // namespace llvm

namespace mlir {

/// Convert the given MLIR module into a C string.
std::string translateModuleToC(Module &m);

} // namespace mlir

#endif // MLIR_TARGET_C_H
