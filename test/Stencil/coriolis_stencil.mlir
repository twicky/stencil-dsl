// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

func @coriolis_stencil(%arg0: !stencil<"field:f64">, %arg1: !stencil<"field:f64">, %arg2: !stencil<"field:f64">, %arg3: !stencil<"field:f64">, %arg4: !stencil<"field:f64">) {
  %0 = stencil.context "kstart" : index
  %1 = stencil.context "kend" : index
  stencil.vertical_region(%0, %1) {

    %off0 = stencil.constant_offset 0 0 0
    %off1 = stencil.constant_offset 0 0 0
    %off2 = stencil.constant_offset 1 0 0
    %2 = stencil.read(%arg4, %off0) : f64
    %3 = stencil.read(%arg3, %off1) : f64
    %4 = stencil.read(%arg3, %off2) : f64
    %5 = stencil.add(%3, %4) : f64
    %6 = stencil.mul(%2, %5) : f64
    %off3 = stencil.constant_offset 0 -1 0
    %off4 = stencil.constant_offset 0 -1 0
    %off5 = stencil.constant_offset 1 -1 0
    %7 = stencil.read(%arg4, %off3) : f64
    %8 = stencil.read(%arg3, %off4) : f64
    %9 = stencil.read(%arg3, %off5) : f64
    %10 = stencil.add(%8, %9) : f64
    %11 = stencil.mul(%7, %10) : f64
    %cst = constant 2.500000e-01 : f64
    %12 = stencil.add(%6, %11) : f64
    %13 = stencil.mul(%cst, %12) : f64
    %off6 = stencil.constant_offset 0 0 0
    %14 = stencil.read(%arg0, %off6) : f64
    %15 = stencil.add(%14, %13) : f64
    stencil.write(%arg0, %15) : f64
    %off7 = stencil.constant_offset 0 0 0
    %off8 = stencil.constant_offset 0 0 0
    %off9 = stencil.constant_offset 0 1 0
    %16 = stencil.read(%arg4, %off7) : f64
    %17 = stencil.read(%arg1, %off8) : f64
    %18 = stencil.read(%arg1, %off9) : f64
    %19 = stencil.add(%17, %18) : f64
    %20 = stencil.mul(%16, %19) : f64
    %off10 = stencil.constant_offset -1 0 0
    %off11 = stencil.constant_offset -1 0 0
    %off12 = stencil.constant_offset -1 1 0
    %21 = stencil.read(%arg4, %off10) : f64
    %22 = stencil.read(%arg1, %off11) : f64
    %23 = stencil.read(%arg1, %off12) : f64
    %24 = stencil.add(%22, %23) : f64
    %25 = stencil.mul(%21, %24) : f64
    %cst_0 = constant 2.500000e-01 : f64
    %26 = stencil.add(%20, %25) : f64
    %27 = stencil.mul(%cst_0, %26) : f64
    %off13 = stencil.constant_offset 0 0 0
    %28 = stencil.read(%arg2, %off13) : f64
    %29 = stencil.sub(%28, %27) : f64
    stencil.write(%arg2, %29) : f64
  }
  return
}

// CHECK-LABEL: @coriolis_stencil
// CHECK:         %0 = stencil.context "kstart" : index
// CHECK-NEXT:    %1 = stencil.context "kend" : index
// CHECK-NEXT:      stencil.vertical_region(%0, %1) {
// CHECK-NEXT:        %2 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %3 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %4 = stencil.constant_offset 1 0 0
// CHECK-NEXT:        %5 = stencil.read(%arg4, %2) : f64
// CHECK-NEXT:        %6 = stencil.read(%arg3, %3) : f64
// CHECK-NEXT:        %7 = stencil.read(%arg3, %4) : f64
// CHECK-NEXT:        %8 = stencil.add(%6, %7) : f64
// CHECK-NEXT:        %9 = stencil.mul(%5, %8) : f64
// CHECK-NEXT:        %10 = stencil.constant_offset 0 -1 0
// CHECK-NEXT:        %11 = stencil.constant_offset 0 -1 0
// CHECK-NEXT:        %12 = stencil.constant_offset 1 -1 0
// CHECK-NEXT:        %13 = stencil.read(%arg4, %10) : f64
// CHECK-NEXT:        %14 = stencil.read(%arg3, %11) : f64
// CHECK-NEXT:        %15 = stencil.read(%arg3, %12) : f64
// CHECK-NEXT:        %16 = stencil.add(%14, %15) : f64
// CHECK-NEXT:        %17 = stencil.mul(%13, %16) : f64
// CHECK-NEXT:        %cst = constant 2.500000e-01 : f64
// CHECK-NEXT:        %18 = stencil.add(%9, %17) : f64
// CHECK-NEXT:        %19 = stencil.mul(%cst, %18) : f64
// CHECK-NEXT:        %20 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %21 = stencil.read(%arg0, %20) : f64
// CHECK-NEXT:        %22 = stencil.add(%21, %19) : f64
// CHECK-NEXT:        stencil.write(%arg0, %22) : f64
// CHECK-NEXT:        %23 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %24 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %25 = stencil.constant_offset 0 1 0
// CHECK-NEXT:        %26 = stencil.read(%arg4, %23) : f64
// CHECK-NEXT:        %27 = stencil.read(%arg1, %24) : f64
// CHECK-NEXT:        %28 = stencil.read(%arg1, %25) : f64
// CHECK-NEXT:        %29 = stencil.add(%27, %28) : f64
// CHECK-NEXT:        %30 = stencil.mul(%26, %29) : f64
// CHECK-NEXT:        %31 = stencil.constant_offset -1 0 0
// CHECK-NEXT:        %32 = stencil.constant_offset -1 0 0
// CHECK-NEXT:        %33 = stencil.constant_offset -1 1 0
// CHECK-NEXT:        %34 = stencil.read(%arg4, %31) : f64
// CHECK-NEXT:        %35 = stencil.read(%arg1, %32) : f64
// CHECK-NEXT:        %36 = stencil.read(%arg1, %33) : f64
// CHECK-NEXT:        %37 = stencil.add(%35, %36) : f64
// CHECK-NEXT:        %38 = stencil.mul(%34, %37) : f64
// CHECK-NEXT:        %cst_0 = constant 2.500000e-01 : f64
// CHECK-NEXT:        %39 = stencil.add(%30, %38) : f64
// CHECK-NEXT:        %40 = stencil.mul(%cst_0, %39) : f64
// CHECK-NEXT:        %41 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %42 = stencil.read(%arg2, %41) : f64
// CHECK-NEXT:        %43 = stencil.sub(%42, %40) : f64
// CHECK-NEXT:        stencil.write(%arg2, %43) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:    return
// CHECK-NEXT:  }
