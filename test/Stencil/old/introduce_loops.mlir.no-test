// RUN: mlir-opt %s -convert-stencils | FileCheck %s

func @hori_diff3(%in : !stencil.field,
                 %out : !stencil.field,
                 %coeff : !stencil.field,
                 %lap :!stencil.field) {
  %kstart = stencil.kstart
  %kend = stencil.kend
  "stencil.vertical_region"(%kstart, %kend) : (index, index) -> () {
    "stencil.evaluate"(%in, %coeff, %lap) : (!stencil.field, !stencil.field, !stencil.field) -> () {
      %0, %1, %2, %3, %in000 = "stencil.read"(%in) {patterns: [[1,0,0], [-1,0,0], [0,1,0], [0,-1,0], [0,0,0]]} : (!stencil.field) -> (f32, f32, f32, f32, f32)
      %c = "stencil.read"(%coeff) {patterns: [[0,0,0]]} : (!stencil.field) -> (f32)
      %sum = "stencil.sum"(%0, %1, %2, %3) : (f32, f32, f32, f32) -> (f32)
      %rhs = "stencil.prod"(%sum, %c) : (f32, f32) -> (f32)
      %cst = constant -4.0 : f32
      %lhs = "stencil.prod"(%cst, %c) : (f32, f32) -> (f32)
      %result = "stencil.sum"(%lhs, %rhs) : (f32, f32) -> (f32)
      "stencil.write"(%result, %lap) {patterns: [[0,0,0]]} : (f32, !stencil.field) -> ()
      return
    }
    stencil.terminator
  }

  return
}

// CHECK-LABEL: @hori_diff
// CHECK:        %0 = stencil.kstart
// CHECK-NEXT:   %1 = stencil.kend
// CHECK-NEXT:   affine.for %i0 = #map0(%0) to #map0(%1) {
// CHECK-NEXT:     "stencil.evaluate"(%arg0, %arg2, %arg3) : (!stencil.field, !stencil.field, !stencil.field) -> () {
// CHECK-NEXT:       %2:5 = "stencil.read"(%arg0)
// CHECK-NEXT:       %3 = "stencil.read"(%arg2)
// CHECK-NEXT:       %4 = "stencil.sum"(%2#0, %2#1, %2#2, %2#3)
// CHECK-NEXT:       %5 = "stencil.prod"(%4, %3)
// CHECK-NEXT:       %cst = constant -4.0
// CHECK-NEXT:       %6 = "stencil.prod"(%cst, %3)
// CHECK-NEXT:       %7 = "stencil.sum"(%6, %5)
// CHECK-NEXT:       "stencil.write"(%7, %arg3)
// CHECK-NEXT:       return
// CHECK-NEXT:     }
// CHECK-NEXT:   }
