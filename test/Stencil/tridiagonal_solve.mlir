// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

#map0 = (d0) -> (d0 + 1)
#map1 = (d0) -> (d0 - 1)

func @tridiagonal_solve(%arg0: !stencil<"field:f64">, %arg1: !stencil<"field:f64">, %arg2: !stencil<"field:f64">, %arg3: !stencil<"field:f64">) {
  %0 = stencil.context "kstart" : index
  %1 = stencil.context "kend" : index
  stencil.vertical_region(%0, %0) {

    %off0 = stencil.constant_offset 0 0 0
    %off1 = stencil.constant_offset 0 0 0
    %2 = stencil.read(%arg3, %off0) : f64
    %3 = stencil.read(%arg2, %off1) : f64
    %4 = stencil.div(%2, %3) : f64
    stencil.write(%arg3, %4) : f64
    %off2 = stencil.constant_offset 0 0 0
    %off3 = stencil.constant_offset 0 0 0
    %5 = stencil.read(%arg0, %off2) : f64
    %6 = stencil.read(%arg2, %off3) : f64
    %7 = stencil.div(%5, %6) : f64
    stencil.write(%arg0, %7) : f64
  }
  %8 = affine.apply #map0(%0)
  stencil.vertical_region(%8, %1) {

    %cst = constant 1.000000e+00 : f64
    %off4 = stencil.constant_offset 0 0 0
    %off5 = stencil.constant_offset 0 0 0
    %off6 = stencil.constant_offset 0 0 -1
    %9 = stencil.read(%arg2, %off4) : f64
    %10 = stencil.read(%arg1, %off5) : f64
    %11 = stencil.read(%arg3, %off6) : f64
    %12 = stencil.mul(%10, %11) : f64
    %13 = stencil.sub(%9, %12) : f64
    %14 = stencil.div(%cst, %13) : f64
    %off7 = stencil.constant_offset 0 0 0
    %15 = stencil.read(%arg3, %off7) : f64
    %16 = stencil.mul(%15, %14) : f64
    stencil.write(%arg3, %16) : f64
    %off8 = stencil.constant_offset 0 0 0
    %off9 = stencil.constant_offset 0 0 0
    %off10 = stencil.constant_offset 0 0 -1
    %17 = stencil.read(%arg0, %off8) : f64
    %18 = stencil.read(%arg1, %off9) : f64
    %19 = stencil.read(%arg0, %off10) : f64
    %20 = stencil.mul(%18, %19) : f64
    %21 = stencil.sub(%17, %20) : f64
    %22 = stencil.mul(%21, %14) : f64
    stencil.write(%arg0, %22) : f64
  }
  %23 = affine.apply #map1(%1)
  stencil.vertical_region(%0, %23) backward {

    %off11 = stencil.constant_offset 0 0 0
    %off12 = stencil.constant_offset 0 0 1
    %24 = stencil.read(%arg3, %off11) : f64
    %25 = stencil.read(%arg0, %off12) : f64
    %26 = stencil.mul(%24, %25) : f64
    %off13 = stencil.constant_offset 0 0 0
    %27 = stencil.read(%arg0, %off13) : f64
    %28 = stencil.sub(%27, %26) : f64
    stencil.write(%arg0, %28) : f64
  }
  return
}

// CHECK-LABEL: @tridiagonal_solve
// CHECK:         %0 = stencil.context "kstart" : index
// CHECK-NEXT:    %1 = stencil.context "kend" : index
// CHECK-NEXT:      stencil.vertical_region(%0, %0) {
// CHECK-NEXT:        %2 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %3 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %4 = stencil.read(%arg3, %2) : f64
// CHECK-NEXT:        %5 = stencil.read(%arg2, %3) : f64
// CHECK-NEXT:        %6 = stencil.div(%4, %5) : f64
// CHECK-NEXT:        stencil.write(%arg3, %6) : f64
// CHECK-NEXT:        %7 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %8 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %9 = stencil.read(%arg0, %7) : f64
// CHECK-NEXT:        %10 = stencil.read(%arg2, %8) : f64
// CHECK-NEXT:        %11 = stencil.div(%9, %10) : f64
// CHECK-NEXT:        stencil.write(%arg0, %11) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %12 = affine.apply #map0(%0)
// CHECK-NEXT:      stencil.vertical_region(%12, %1) {
// CHECK-NEXT:        %cst = constant 1.000000e+00 : f64
// CHECK-NEXT:        %13 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %14 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %15 = stencil.constant_offset 0 0 -1
// CHECK-NEXT:        %16 = stencil.read(%arg2, %13) : f64
// CHECK-NEXT:        %17 = stencil.read(%arg1, %14) : f64
// CHECK-NEXT:        %18 = stencil.read(%arg3, %15) : f64
// CHECK-NEXT:        %19 = stencil.mul(%17, %18) : f64
// CHECK-NEXT:        %20 = stencil.sub(%16, %19) : f64
// CHECK-NEXT:        %21 = stencil.div(%cst, %20) : f64
// CHECK-NEXT:        %22 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %23 = stencil.read(%arg3, %22) : f64
// CHECK-NEXT:        %24 = stencil.mul(%23, %21) : f64
// CHECK-NEXT:        stencil.write(%arg3, %24) : f64
// CHECK-NEXT:        %25 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %26 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %27 = stencil.constant_offset 0 0 -1
// CHECK-NEXT:        %28 = stencil.read(%arg0, %25) : f64
// CHECK-NEXT:        %29 = stencil.read(%arg1, %26) : f64
// CHECK-NEXT:        %30 = stencil.read(%arg0, %27) : f64
// CHECK-NEXT:        %31 = stencil.mul(%29, %30) : f64
// CHECK-NEXT:        %32 = stencil.sub(%28, %31) : f64
// CHECK-NEXT:        %33 = stencil.mul(%32, %21) : f64
// CHECK-NEXT:        stencil.write(%arg0, %33) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %34 = affine.apply #map1(%1)
// CHECK-NEXT:      stencil.vertical_region(%0, %34) backward {
// CHECK-NEXT:        %35 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %36 = stencil.constant_offset 0 0 1
// CHECK-NEXT:        %37 = stencil.read(%arg3, %35) : f64
// CHECK-NEXT:        %38 = stencil.read(%arg0, %36) : f64
// CHECK-NEXT:        %39 = stencil.mul(%37, %38) : f64
// CHECK-NEXT:        %40 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %41 = stencil.read(%arg0, %40) : f64
// CHECK-NEXT:        %42 = stencil.sub(%41, %39) : f64
// CHECK-NEXT:        stencil.write(%arg0, %42) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:    return
// CHECK-NEXT:  }
