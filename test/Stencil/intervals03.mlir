// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

#map0 = (d0) -> (d0 - 1)
#map1 = (d0) -> (d0 + 4)
#map2 = (d0) -> (d0 - 2)
#map3 = (d0) -> (d0 + 2)
#map4 = (d0) -> (d0 + 1)

func @intervals03(%arg0: !stencil<"field:f64">, %arg1: !stencil<"field:f64">) {
  %0 = stencil.context "kstart" : index
  %1 = stencil.context "kend" : index
  %2 = affine.apply #map0(%1)
  %3 = affine.apply #map0(%1)
  stencil.vertical_region(%2, %3) {
    %cst = constant 0.000000e+00 : f64
    stencil.write(%arg1, %cst) : f64
  }
  %4 = affine.apply #map1(%0)
  %5 = affine.apply #map2(%1)
  stencil.vertical_region(%4, %5) backward {
    %off0 = stencil.constant_offset 0 0 1
    %off1 = stencil.constant_offset 0 0 0
    %6 = stencil.read(%arg1, %off0) : f64
    %7 = stencil.read(%arg0, %off1) : f64
    %8 = stencil.add(%6, %7) : f64
    %cst_0 = constant 3.000000e+00 : f64
    %9 = stencil.add(%8, %cst_0) : f64
    stencil.write(%arg1, %9) : f64
  }
  %10 = affine.apply #map3(%0)
  %11 = affine.apply #map3(%0)
  stencil.vertical_region(%10, %11) {
    %cst_1 = constant 1.000000e+00 : f64
    stencil.write(%arg1, %cst_1) : f64
  }
  %12 = affine.apply #map4(%0)
  stencil.vertical_region(%0, %12) backward {
    %off2 = stencil.constant_offset 0 0 1
    %off3 = stencil.constant_offset 0 0 0
    %13 = stencil.read(%arg1, %off2) : f64
    %14 = stencil.read(%arg0, %off3) : f64
    %15 = stencil.add(%13, %14) : f64
    %cst_2 = constant 3.000000e+00 : f64
    %16 = stencil.add(%15, %cst_2) : f64
    stencil.write(%arg1, %16) : f64
  }
  return
}

// CHECK-LABEL: @intervals03
// CHECK:         %0 = stencil.context "kstart" : index
// CHECK-NEXT:    %1 = stencil.context "kend" : index
// CHECK-NEXT:      %2 = affine.apply #map0(%1)
// CHECK-NEXT:      %3 = affine.apply #map0(%1)
// CHECK-NEXT:      stencil.vertical_region(%2, %3) {
// CHECK-NEXT:        %cst = constant 0.000000e+00 : f64
// CHECK-NEXT:        stencil.write(%arg1, %cst) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %4 = affine.apply #map1(%0)
// CHECK-NEXT:      %5 = affine.apply #map2(%1)
// CHECK-NEXT:      stencil.vertical_region(%4, %5) backward {
// CHECK-NEXT:        %6 = stencil.constant_offset 0 0 1
// CHECK-NEXT:        %7 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %8 = stencil.read(%arg1, %6) : f64
// CHECK-NEXT:        %9 = stencil.read(%arg0, %7) : f64
// CHECK-NEXT:        %10 = stencil.add(%8, %9) : f64
// CHECK-NEXT:        %cst_0 = constant 3.000000e+00 : f64
// CHECK-NEXT:        %11 = stencil.add(%10, %cst_0) : f64
// CHECK-NEXT:        stencil.write(%arg1, %11) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %12 = affine.apply #map3(%0)
// CHECK-NEXT:      %13 = affine.apply #map3(%0)
// CHECK-NEXT:      stencil.vertical_region(%12, %13) {
// CHECK-NEXT:        %cst_1 = constant 1.000000e+00 : f64
// CHECK-NEXT:        stencil.write(%arg1, %cst_1) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %14 = affine.apply #map4(%0)
// CHECK-NEXT:      stencil.vertical_region(%0, %14) backward {
// CHECK-NEXT:        %15 = stencil.constant_offset 0 0 1
// CHECK-NEXT:        %16 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %17 = stencil.read(%arg1, %15) : f64
// CHECK-NEXT:        %18 = stencil.read(%arg0, %16) : f64
// CHECK-NEXT:        %19 = stencil.add(%17, %18) : f64
// CHECK-NEXT:        %cst_2 = constant 3.000000e+00 : f64
// CHECK-NEXT:        %20 = stencil.add(%19, %cst_2) : f64
// CHECK-NEXT:        stencil.write(%arg1, %20) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:    return
// CHECK-NEXT:  }
