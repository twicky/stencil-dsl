// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

func @copy_stencil(%in : !stencil<"field:f64">,
                   %out : !stencil<"field:f64">) {
    %kstart = stencil.context "kstart" : index
    %kend = stencil.context "kend" : index
    stencil.vertical_region(%kstart, %kend) {
        %off = stencil.constant_offset 0 0 0
        %0 = stencil.read(%in, %off) : f64
        stencil.write(%out, %0) : f64
    }

    return
}

// CHECK-LABEL: @copy_stencil
// CHECK:         %0 = stencil.context "kstart" : index
// CHECK-NEXT:    %1 = stencil.context "kend" : index
// CHECK-NEXT:      stencil.vertical_region(%0, %1) {
// CHECK-NEXT:        %2 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %3 = stencil.read(%arg0, %2) : f64
// CHECK-NEXT:        stencil.write(%arg1, %3) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:    return
// CHECK-NEXT:  }
