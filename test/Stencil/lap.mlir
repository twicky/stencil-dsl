// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

func @lap(%in : !stencil<"field:f64">,
          %out : !stencil<"field:f64">) {
     %tmp = stencil.temp : !stencil<"field:f64">

     %kstart = stencil.context "kstart" : index
     %kend = stencil.context "kend" : index

     stencil.vertical_region(%kstart, %kend) {

         %off0 = stencil.constant_offset 0 -2 0
         %off1 = stencil.constant_offset 0 2 0
         %off2 = stencil.constant_offset -2 0 0
         %off3 = stencil.constant_offset 2 0 0
         %0 = stencil.read(%in, %off0) : f64
         %1 = stencil.read(%in, %off1) : f64
         %2 = stencil.read(%in, %off2) : f64
         %3 = stencil.read(%in, %off3) : f64
         %sum1 = stencil.add(%0, %1) : f64
         %sum2 = stencil.add(%2, %3) : f64
         %sum3 = stencil.add(%sum1, %sum2) : f64
         stencil.write(%tmp, %sum3) : f64

         %off4 = stencil.constant_offset 1 0 0
         %off5 = stencil.constant_offset 0 1 0
         %off6 = stencil.constant_offset -1 0 0
         %off7 = stencil.constant_offset 0 -1 0
         %4 = stencil.read(%tmp, %off4) : f64
         %5 = stencil.read(%tmp, %off5) : f64
         %6 = stencil.read(%tmp, %off6) : f64
         %7 = stencil.read(%tmp, %off7) : f64
         %sum4 = stencil.add(%4, %5) : f64
         %sum5 = stencil.add(%6, %7) : f64
         %sum6 = stencil.add(%sum4, %sum5) : f64
         stencil.write(%out, %sum6) : f64
     }

     return
}

// CHECK-LABEL: @lap
// CHECK:         %0 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %1 = stencil.context "kstart" : index
// CHECK-NEXT:    %2 = stencil.context "kend" : index
// CHECK-NEXT:      stencil.vertical_region(%1, %2) {
// CHECK-NEXT:        %3 = stencil.constant_offset 0 -2 0
// CHECK-NEXT:        %4 = stencil.constant_offset 0 2 0
// CHECK-NEXT:        %5 = stencil.constant_offset -2 0 0
// CHECK-NEXT:        %6 = stencil.constant_offset 2 0 0
// CHECK-NEXT:        %7 = stencil.read(%arg0, %3) : f64
// CHECK-NEXT:        %8 = stencil.read(%arg0, %4) : f64
// CHECK-NEXT:        %9 = stencil.read(%arg0, %5) : f64
// CHECK-NEXT:        %10 = stencil.read(%arg0, %6) : f64
// CHECK-NEXT:        %11 = stencil.add(%7, %8) : f64
// CHECK-NEXT:        %12 = stencil.add(%9, %10) : f64
// CHECK-NEXT:        %13 = stencil.add(%11, %12) : f64
// CHECK-NEXT:        stencil.write(%0, %13) : f64
// CHECK-NEXT:        %14 = stencil.constant_offset 1 0 0
// CHECK-NEXT:        %15 = stencil.constant_offset 0 1 0
// CHECK-NEXT:        %16 = stencil.constant_offset -1 0 0
// CHECK-NEXT:        %17 = stencil.constant_offset 0 -1 0
// CHECK-NEXT:        %18 = stencil.read(%0, %14) : f64
// CHECK-NEXT:        %19 = stencil.read(%0, %15) : f64
// CHECK-NEXT:        %20 = stencil.read(%0, %16) : f64
// CHECK-NEXT:        %21 = stencil.read(%0, %17) : f64
// CHECK-NEXT:        %22 = stencil.add(%18, %19) : f64
// CHECK-NEXT:        %23 = stencil.add(%20, %21) : f64
// CHECK-NEXT:        %24 = stencil.add(%22, %23) : f64
// CHECK-NEXT:        stencil.write(%arg1, %24) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:    return
// CHECK-NEXT:  }
