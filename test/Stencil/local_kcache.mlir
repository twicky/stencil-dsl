// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

#map0 = (d0) -> (d0 + 1)
#map1 = (d0) -> (d0 + 2)
#map2 = (d0) -> (d0 - 1)
#map3 = (d0) -> (d0 - 2)

func @local_kcache(%arg0: !stencil<"field:f64">, %arg1: !stencil<"field:f64">, %arg2: !stencil<"field:f64">, %arg3: !stencil<"field:f64">) {
  %0 = stencil.temp : !stencil<"field:f64">
  %1 = stencil.temp : !stencil<"field:f64">
  %2 = stencil.temp : !stencil<"field:f64">
  %3 = stencil.temp : !stencil<"field:f64">
  %4 = stencil.context "kstart" : index
  %5 = stencil.context "kend" : index

  stencil.vertical_region(%4, %4) {
    %cst = constant 2.100000e+00 : f64
    stencil.write(%0, %cst) : f64
    %cst_0 = constant 3.100000e+00 : f64
    stencil.write(%2, %cst_0) : f64
    %off0 = stencil.constant_offset 0 0 0
    %6 = stencil.read(%0, %off0) : f64
    stencil.write(%arg2, %6) : f64
    %off1 = stencil.constant_offset 0 0 0
    %7 = stencil.read(%2, %off1) : f64
    stencil.write(%arg3, %7) : f64
  }
  %8 = affine.apply #map0(%4)
  %9 = affine.apply #map0(%4)
  stencil.vertical_region(%8, %9) {

    %cst_1 = constant 4.100000e+00 : f64
    stencil.write(%2, %cst_1) : f64
    %off2 = stencil.constant_offset 0 0 0
    %10 = stencil.read(%2, %off2) : f64
    stencil.write(%arg3, %10) : f64
    %off3 = stencil.constant_offset 0 0 0
    %11 = stencil.read(%0, %off3) : f64
    stencil.write(%arg2, %11) : f64
  }
  %12 = affine.apply #map1(%4)
  stencil.vertical_region(%12, %5) {

    %off4 = stencil.constant_offset 0 0 -1
    %13 = stencil.read(%0, %off4) : f64
    %cst_2 = constant 1.100000e+00 : f64
    %14 = stencil.mul(%13, %cst_2) : f64
    stencil.write(%0, %14) : f64
    %off5 = stencil.constant_offset 0 0 -1
    %15 = stencil.read(%2, %off5) : f64
    %cst_3 = constant 1.100000e+00 : f64
    %16 = stencil.mul(%15, %cst_3) : f64
    %off6 = stencil.constant_offset 0 0 -2
    %17 = stencil.read(%2, %off6) : f64
    %cst_4 = constant 1.200000e+00 : f64
    %18 = stencil.mul(%17, %cst_4) : f64
    %19 = stencil.add(%16, %18) : f64
    stencil.write(%2, %19) : f64
    %off7 = stencil.constant_offset 0 0 0
    %20 = stencil.read(%0, %off7) : f64
    stencil.write(%arg2, %20) : f64
    %off8 = stencil.constant_offset 0 0 0
    %21 = stencil.read(%2, %off8) : f64
    stencil.write(%arg3, %21) : f64
  }
  stencil.vertical_region(%5, %5) {

    %cst_5 = constant 2.100000e+00 : f64
    stencil.write(%3, %cst_5) : f64
    %cst_6 = constant 3.100000e+00 : f64
    stencil.write(%1, %cst_6) : f64
    %off9 = stencil.constant_offset 0 0 0
    %22 = stencil.read(%3, %off9) : f64
    stencil.write(%arg0, %22) : f64
    %off10 = stencil.constant_offset 0 0 0
    %23 = stencil.read(%1, %off10) : f64
    stencil.write(%arg1, %23) : f64
  }
  %24 = affine.apply #map2(%5)
  %25 = affine.apply #map2(%5)
  stencil.vertical_region(%24, %25) {

    %cst_7 = constant 4.100000e+00 : f64
    stencil.write(%1, %cst_7) : f64
    %off11 = stencil.constant_offset 0 0 0
    %26 = stencil.read(%1, %off11) : f64
    stencil.write(%arg1, %26) : f64
    %off12 = stencil.constant_offset 0 0 0
    %27 = stencil.read(%3, %off12) : f64
    stencil.write(%arg0, %27) : f64
  }
  %28 = affine.apply #map3(%5)
  stencil.vertical_region(%4, %28) backward {

    %off13 = stencil.constant_offset 0 0 1
    %29 = stencil.read(%3, %off13) : f64
    %cst_8 = constant 1.100000e+00 : f64
    %30 = stencil.mul(%29, %cst_8) : f64
    stencil.write(%3, %30) : f64
    %off14 = stencil.constant_offset 0 0 1
    %31 = stencil.read(%1, %off14) : f64
    %cst_9 = constant 1.100000e+00 : f64
    %32 = stencil.mul(%31, %cst_9) : f64
    %off15 = stencil.constant_offset 0 0 2
    %33 = stencil.read(%1, %off15) : f64
    %cst_10 = constant 1.200000e+00 : f64
    %34 = stencil.mul(%33, %cst_10) : f64
    %35 = stencil.add(%32, %34) : f64
    stencil.write(%1, %35) : f64
    %off16 = stencil.constant_offset 0 0 0
    %36 = stencil.read(%3, %off16) : f64
    stencil.write(%arg0, %36) : f64
    %off17 = stencil.constant_offset 0 0 0
    %37 = stencil.read(%1, %off17) : f64
    stencil.write(%arg1, %37) : f64
  }
  return
}

// CHECK-LABEL: @local_kcache
// CHECK:         %0 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %1 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %2 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %3 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %4 = stencil.context "kstart" : index
// CHECK-NEXT:    %5 = stencil.context "kend" : index
// CHECK-NEXT:      stencil.vertical_region(%4, %4) {
// CHECK-NEXT:        %cst = constant 2.100000e+00 : f64
// CHECK-NEXT:        stencil.write(%0, %cst) : f64
// CHECK-NEXT:        %cst_0 = constant 3.100000e+00 : f64
// CHECK-NEXT:        stencil.write(%2, %cst_0) : f64
// CHECK-NEXT:        %6 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %7 = stencil.read(%0, %6) : f64
// CHECK-NEXT:        stencil.write(%arg2, %7) : f64
// CHECK-NEXT:        %8 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %9 = stencil.read(%2, %8) : f64
// CHECK-NEXT:        stencil.write(%arg3, %9) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %10 = affine.apply #map0(%4)
// CHECK-NEXT:      %11 = affine.apply #map0(%4)
// CHECK-NEXT:      stencil.vertical_region(%10, %11) {
// CHECK-NEXT:        %cst_1 = constant 4.100000e+00 : f64
// CHECK-NEXT:        stencil.write(%2, %cst_1) : f64
// CHECK-NEXT:        %12 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %13 = stencil.read(%2, %12) : f64
// CHECK-NEXT:        stencil.write(%arg3, %13) : f64
// CHECK-NEXT:        %14 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %15 = stencil.read(%0, %14) : f64
// CHECK-NEXT:        stencil.write(%arg2, %15) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %16 = affine.apply #map1(%4)
// CHECK-NEXT:      stencil.vertical_region(%16, %5) {
// CHECK-NEXT:        %17 = stencil.constant_offset 0 0 -1
// CHECK-NEXT:        %18 = stencil.read(%0, %17) : f64
// CHECK-NEXT:        %cst_2 = constant 1.100000e+00 : f64
// CHECK-NEXT:        %19 = stencil.mul(%18, %cst_2) : f64
// CHECK-NEXT:        stencil.write(%0, %19) : f64
// CHECK-NEXT:        %20 = stencil.constant_offset 0 0 -1
// CHECK-NEXT:        %21 = stencil.read(%2, %20) : f64
// CHECK-NEXT:        %cst_3 = constant 1.100000e+00 : f64
// CHECK-NEXT:        %22 = stencil.mul(%21, %cst_3) : f64
// CHECK-NEXT:        %23 = stencil.constant_offset 0 0 -2
// CHECK-NEXT:        %24 = stencil.read(%2, %23) : f64
// CHECK-NEXT:        %cst_4 = constant 1.200000e+00 : f64
// CHECK-NEXT:        %25 = stencil.mul(%24, %cst_4) : f64
// CHECK-NEXT:        %26 = stencil.add(%22, %25) : f64
// CHECK-NEXT:        stencil.write(%2, %26) : f64
// CHECK-NEXT:        %27 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %28 = stencil.read(%0, %27) : f64
// CHECK-NEXT:        stencil.write(%arg2, %28) : f64
// CHECK-NEXT:        %29 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %30 = stencil.read(%2, %29) : f64
// CHECK-NEXT:        stencil.write(%arg3, %30) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      stencil.vertical_region(%5, %5) {
// CHECK-NEXT:        %cst_5 = constant 2.100000e+00 : f64
// CHECK-NEXT:        stencil.write(%3, %cst_5) : f64
// CHECK-NEXT:        %cst_6 = constant 3.100000e+00 : f64
// CHECK-NEXT:        stencil.write(%1, %cst_6) : f64
// CHECK-NEXT:        %31 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %32 = stencil.read(%3, %31) : f64
// CHECK-NEXT:        stencil.write(%arg0, %32) : f64
// CHECK-NEXT:        %33 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %34 = stencil.read(%1, %33) : f64
// CHECK-NEXT:        stencil.write(%arg1, %34) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %35 = affine.apply #map2(%5)
// CHECK-NEXT:      %36 = affine.apply #map2(%5)
// CHECK-NEXT:      stencil.vertical_region(%35, %36) {
// CHECK-NEXT:        %cst_7 = constant 4.100000e+00 : f64
// CHECK-NEXT:        stencil.write(%1, %cst_7) : f64
// CHECK-NEXT:        %37 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %38 = stencil.read(%1, %37) : f64
// CHECK-NEXT:        stencil.write(%arg1, %38) : f64
// CHECK-NEXT:        %39 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %40 = stencil.read(%3, %39) : f64
// CHECK-NEXT:        stencil.write(%arg0, %40) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %41 = affine.apply #map3(%5)
// CHECK-NEXT:      stencil.vertical_region(%4, %41) backward {
// CHECK-NEXT:        %42 = stencil.constant_offset 0 0 1
// CHECK-NEXT:        %43 = stencil.read(%3, %42) : f64
// CHECK-NEXT:        %cst_8 = constant 1.100000e+00 : f64
// CHECK-NEXT:        %44 = stencil.mul(%43, %cst_8) : f64
// CHECK-NEXT:        stencil.write(%3, %44) : f64
// CHECK-NEXT:        %45 = stencil.constant_offset 0 0 1
// CHECK-NEXT:        %46 = stencil.read(%1, %45) : f64
// CHECK-NEXT:        %cst_9 = constant 1.100000e+00 : f64
// CHECK-NEXT:        %47 = stencil.mul(%46, %cst_9) : f64
// CHECK-NEXT:        %48 = stencil.constant_offset 0 0 2
// CHECK-NEXT:        %49 = stencil.read(%1, %48) : f64
// CHECK-NEXT:        %cst_10 = constant 1.200000e+00 : f64
// CHECK-NEXT:        %50 = stencil.mul(%49, %cst_10) : f64
// CHECK-NEXT:        %51 = stencil.add(%47, %50) : f64
// CHECK-NEXT:        stencil.write(%1, %51) : f64
// CHECK-NEXT:        %52 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %53 = stencil.read(%3, %52) : f64
// CHECK-NEXT:        stencil.write(%arg0, %53) : f64
// CHECK-NEXT:        %54 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %55 = stencil.read(%1, %54) : f64
// CHECK-NEXT:        stencil.write(%arg1, %55) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:    return
// CHECK-NEXT:  }
