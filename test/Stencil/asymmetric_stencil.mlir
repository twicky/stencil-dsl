// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

#map0 = (d0) -> (d0 + 1)

func @asymmetric_stencil(%arg0: !stencil<"field:f64">, %arg1: !stencil<"field:f64">) {
  %0 = stencil.temp : !stencil<"field:f64">
  %1 = stencil.context "kstart" : index
  %2 = stencil.context "kend" : index

  stencil.vertical_region(%1, %1) {
    %off0 = stencil.constant_offset 1 0 0
    %off1 = stencil.constant_offset 0 -2 0
    %3 = stencil.read(%arg0, %off0) : f64
    %4 = stencil.read(%arg0, %off1) : f64
    %5 = stencil.add(%3, %4) : f64
    stencil.write(%0, %5) : f64
    %cst = constant 1.000000e+00 : f64
    stencil.write(%arg1, %cst) : f64
  }
  %c1 = constant 1 : index
  %6 = affine.apply #map0(%1)
  stencil.vertical_region(%6, %2) {
    %off2 = stencil.constant_offset -3 0 0
    %off3 = stencil.constant_offset 0 1 0
    %7 = stencil.read(%arg0, %off2) : f64
    %8 = stencil.read(%arg0, %off3) : f64
    %9 = stencil.add(%7, %8) : f64
    stencil.write(%0, %9) : f64
    %off4 = stencil.constant_offset 0 0 -1
    %10 = stencil.read(%0, %off4) : f64
    stencil.write(%arg1, %10) : f64
  }
  return
}

// CHECK-LABEL: asymmetric_stencil
// CHECK:         %0 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %1 = stencil.context "kstart" : index
// CHECK-NEXT:    %2 = stencil.context "kend" : index
// CHECK-NEXT:      stencil.vertical_region(%1, %1) {
// CHECK-NEXT:        %3 = stencil.constant_offset 1 0 0
// CHECK-NEXT:        %4 = stencil.constant_offset 0 -2 0
// CHECK-NEXT:        %5 = stencil.read(%arg0, %3) : f64
// CHECK-NEXT:        %6 = stencil.read(%arg0, %4) : f64
// CHECK-NEXT:        %7 = stencil.add(%5, %6) : f64
// CHECK-NEXT:        stencil.write(%0, %7) : f64
// CHECK-NEXT:        %cst = constant 1.000000e+00 : f64
// CHECK-NEXT:        stencil.write(%arg1, %cst) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:      %c1 = constant 1 : index
// CHECK-NEXT:      %8 = affine.apply #map0(%1)
// CHECK-NEXT:      stencil.vertical_region(%8, %2) {
// CHECK-NEXT:        %9 = stencil.constant_offset -3 0 0
// CHECK-NEXT:        %10 = stencil.constant_offset 0 1 0
// CHECK-NEXT:        %11 = stencil.read(%arg0, %9) : f64
// CHECK-NEXT:        %12 = stencil.read(%arg0, %10) : f64
// CHECK-NEXT:        %13 = stencil.add(%11, %12) : f64
// CHECK-NEXT:        stencil.write(%0, %13) : f64
// CHECK-NEXT:        %14 = stencil.constant_offset 0 0 -1
// CHECK-NEXT:        %15 = stencil.read(%0, %14) : f64
// CHECK-NEXT:        stencil.write(%arg1, %15) : f64
// CHECK-NEXT:      }
// CHECK-NEXT:    return
// CHECK-NEXT:  }
