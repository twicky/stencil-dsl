//===- COps.cpp - Implementation of the C operations ----------===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements a the Linalg operations.
//
//===----------------------------------------------------------------------===//

#include "mlir/C/COps.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Linalg/IR/LinalgTypes.h"
#include "mlir/StandardOps/Ops.h"
#include "mlir/Support/LLVM.h"
#include "mlir/Support/STLExtras.h"

using namespace mlir;

namespace mlir {
namespace impl {

void printCScalarOp(mlir::OpAsmPrinter *p, Operation *op) {
  const auto *abstract = op->getAbstractOperation();
  assert(abstract && "pretty-printing an unregistered operation");

  *p << op->getName().getStringRef() << "(" << *op->getOperand(0) << ", "
     << *op->getOperand(1) << ")";
  p->printOptionalAttrDict(op->getAttrs());
  *p << " : " << op->getResult(0)->getType();
}

void printCComparisonOp(mlir::OpAsmPrinter *p, Operation *op) {
  const auto *abstract = op->getAbstractOperation();
  assert(abstract && "pretty-printing an unregistered operation");

  *p << op->getName().getStringRef() << "(" << *op->getOperand(0) << ", "
     << *op->getOperand(1) << ")";
  p->printOptionalAttrDict(op->getAttrs());
  *p << " : " << op->getOperand(0)->getType();
}

ParseResult parseCScalarOp(OpAsmParser *parser, OperationState *result) {
  SmallVector<OpAsmParser::OperandType, 2> ops;
  Type type;
  bool x = parser->parseOperandList(ops, 2, OpAsmParser::Delimiter::Paren) ||
           parser->parseColonType(type) ||
           parser->resolveOperands(ops, type, result->operands) ||
           parser->addTypeToList(type, result->types);

  return failure(x);
}

ParseResult parseCComparisonOp(OpAsmParser *parser, OperationState *result) {
  SmallVector<OpAsmParser::OperandType, 2> ops;
  Type type;
  bool x =
      parser->parseOperandList(ops, 2, OpAsmParser::Delimiter::Paren) ||
      parser->parseColonType(type) ||
      parser->resolveOperands(ops, type, result->operands) ||
      parser->addTypeToList(parser->getBuilder().getI1Type(), result->types);

  return failure(x);
}

} // namespace impl

namespace C {

#define GET_OP_CLASSES
#include "mlir/C/COps.cpp.inc"

void ForOp::build(Builder *builder, OperationState *result, Value *LowerBound,
                  Value *UpperBound) {

  result->addOperands({LowerBound, UpperBound});

  // Reserve one region for the loop body
  result->regions.reserve(1);
  result->addRegion(nullptr);
}

LogicalResult ForOp::verify() { return success(); }

ParseResult ForOp::parse(OpAsmParser *parser, OperationState *result) {
  auto &builder = parser->getBuilder();
  OpAsmParser::OperandType InductionVariable, LowerBound, UpperBound;
  result->regions.reserve(1);
  Region *bodyRegion = result->addRegion();
  Type type = IndexType::get(builder.getContext());
  bool ok = parser->parseLParen() ||
            parser->parseRegionArgument(InductionVariable) ||
            parser->parseEqual() || parser->parseOperand(LowerBound) ||
            parser->parseKeyword("to", " between bounds") ||
            parser->parseOperand(UpperBound) || parser->parseRParen() ||
            parser->parseRegion(*bodyRegion, InductionVariable, type);

  ok = ok || parser->resolveOperand(LowerBound, type, result->operands);
  ok = ok || parser->resolveOperand(UpperBound, type, result->operands);

  if (bodyRegion->empty())
    bodyRegion->push_back(new Block);

  Block &block = bodyRegion->back();
  if (!block.empty() && block.back().isKnownTerminator())
    return failure(ok);

  OperationState terminatorState(result->location,
                                 ReturnOp::getOperationName());
  ReturnOp::build(&builder, &terminatorState);
  block.push_back(Operation::create(terminatorState));

  return failure(ok);
}

void ForOp::print(OpAsmPrinter *p) {

  *p << "C.for (";
  p->printOperand(getBody().front().getArgument(0));
  *p << " = ";
  p->printOperand(getLowerBound());
  *p << " to ";
  p->printOperand(getUpperBound());
  *p << ")";
  p->printRegion(getBody(), false, false);
}

/// Returns the list of 'then' blocks.
Region &ForOp::getBody() { return getOperation()->getRegion(0); }

} // namespace C

} // namespace mlir
