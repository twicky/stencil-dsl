//===- Dialect.cpp - Implementation of the linalg dialect and types -------===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements the C dialect types and dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/C/CTypes.h"
#include "mlir/C/COps.h"
#include "mlir/IR/Dialect.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Support/LLVM.h"

using namespace mlir;

mlir::CDialect::CDialect(MLIRContext *context)
    : Dialect(getDialectNamespace(), context) {
  addTypes<C::VoidPtrType>();
  addOperations<
#define GET_OP_LIST
#include "mlir/C/COps.cpp.inc"
      >();
  addOperations<C::ForOp>();
  allowUnknownOperations();
}

Type mlir::CDialect::parseType(StringRef spec, Location loc) const {
  MLIRContext *context = getContext();
  if (spec == "voidptr")
    return C::VoidPtrType::get(getContext());
  return (emitError(loc, "unknown c type: " + spec), Type());
}

static void print(mlir::C::VoidPtrType ft, raw_ostream &os) { os << "voidptr"; }

void mlir::CDialect::printType(Type type, raw_ostream &os) const {
  switch (type.getKind()) {
  default:
    llvm_unreachable("Unhandled C type");
  case C::CVoidPtrTy:
    print(type.cast<C::VoidPtrType>(), os);
    break;
  }
}
