add_llvm_library(MLIRStencil
        StencilOps.cpp
        StencilRegistration.cpp
        StencilTypes.cpp
        StencilToCLowering.cpp
        StencilWriteSplitting.cpp
        StencilVerticalRegionToLoops.cpp
        StencilInlineLambda.cpp

        ADDITIONAL_HEADER_DIRS
        ${MLIR_MAIN_INCLUDE_DIR}/mlir/Linalg
        )
add_dependencies(MLIRStencil MLIRStencilOpsIncGen)
