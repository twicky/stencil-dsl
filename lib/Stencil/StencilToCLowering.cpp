#include "mlir/AffineOps/AffineOps.h"
#include "mlir/C/COps.h"
#include "mlir/C/CTypes.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/AffineExprVisitor.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/BlockAndValueMapping.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Linalg/IR/LinalgTypes.h"
#include "mlir/Pass/Pass.h"
#include "mlir/StandardOps/Ops.h"
#include "mlir/Stencil/StencilOps.h"
#include "mlir/Support/Functional.h"
#include "mlir/Support/LLVM.h"
#include "mlir/Support/STLExtras.h"
#include "mlir/Transforms/DialectConversion.h"

using namespace mlir;

namespace {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Stencil to C lowering

class AffineApplyExpander
    : public AffineExprVisitor<AffineApplyExpander, Value *> {
public:
  AffineApplyExpander(OpBuilder *builder, ArrayRef<Value *> dimValues,
                      ArrayRef<Value *> symbolValues, Location loc)
      : builder(*builder), dimValues(dimValues), symbolValues(symbolValues),
        loc(loc) {}

  template <typename OpTy>
  Value *buildBinaryExpr(AffineBinaryOpExpr expr) {
    auto lhs = visit(expr.getLHS());
    auto rhs = visit(expr.getRHS());
    if (!lhs || !rhs)
      return nullptr;
    auto op = builder.create<OpTy>(loc, lhs, rhs);
    return op.getResult();
  }

  Value *visitAddExpr(AffineBinaryOpExpr expr) {
    return buildBinaryExpr<C::AddIOp>(expr);
  }

  Value *visitMulExpr(AffineBinaryOpExpr expr) {
    return buildBinaryExpr<C::MulIOp>(expr);
  }

  Value *visitModExpr(AffineBinaryOpExpr expr) {
    emitError(loc, "modulo expressions are not supported");
    return nullptr;
  }

  Value *visitFloorDivExpr(AffineBinaryOpExpr expr) {
    emitError(loc, "floor division expressions are not supported");
    return nullptr;
  }

  Value *visitCeilDivExpr(AffineBinaryOpExpr expr) {
    emitError(loc, "ceil division expressions are not supported");
    return nullptr;
  }

  Value *visitConstantExpr(AffineConstantExpr expr) {
    auto valueAttr =
        builder.getIntegerAttr(builder.getIndexType(), expr.getValue());
    auto op =
        builder.create<ConstantOp>(loc, builder.getIndexType(), valueAttr);
    return op.getResult();
  }

  Value *visitDimExpr(AffineDimExpr expr) {
    assert(expr.getPosition() < dimValues.size() &&
           "affine dim position out of range");
    return dimValues[expr.getPosition()];
  }

  Value *visitSymbolExpr(AffineSymbolExpr expr) {
    assert(expr.getPosition() < symbolValues.size() &&
           "symbol dim position out of range");
    return symbolValues[expr.getPosition()];
  }

private:
  OpBuilder &builder;
  ArrayRef<Value *> dimValues;
  ArrayRef<Value *> symbolValues;

  Location loc;
};

} // namespace

static mlir::Value *expandAffineExpr(OpBuilder *builder, Location loc,
                                     AffineExpr expr,
                                     ArrayRef<Value *> dimValues,
                                     ArrayRef<Value *> symbolValues) {
  return AffineApplyExpander(builder, dimValues, symbolValues, loc).visit(expr);
}

Optional<SmallVector<Value *, 8>> static expandAffineMap(
    OpBuilder *builder, Location loc, AffineMap affineMap,
    ArrayRef<Value *> operands) {
  auto numDims = affineMap.getNumDims();
  auto expanded = functional::map(
      [numDims, builder, loc, operands](AffineExpr expr) {
        return expandAffineExpr(builder, loc, expr,
                                operands.take_front(numDims),
                                operands.drop_front(numDims));
      },
      affineMap.getResults());
  if (llvm::all_of(expanded, [](Value *v) { return v; }))
    return expanded;
  return None;
}

namespace {

class AddOpConversion : public ConversionPattern {
public:
  AddOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::AddOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Type resType = op->getResult(0)->getType();
    if (resType.isF32() || resType.isF64()) {
      auto addOp =
          rewriter.create<C::AddFOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, addOp.getResult());
    } else {
      auto addOp =
          rewriter.create<C::AddIOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, addOp.getResult());
    }
    return matchSuccess();
  }
};

class SubOpConversion : public ConversionPattern {
public:
  SubOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::SubOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Type resType = op->getResult(0)->getType();
    if (resType.isF32() || resType.isF64()) {
      auto subOp =
          rewriter.create<C::SubFOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, subOp.getResult());
    } else {
      auto subOp =
          rewriter.create<C::SubIOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, subOp.getResult());
    }
    return matchSuccess();
  }
};

class MulOpConversion : public ConversionPattern {
public:
  MulOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::MulOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Type resType = op->getResult(0)->getType();
    if (resType.isF32() || resType.isF64()) {
      auto mulOp =
          rewriter.create<C::MulFOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, mulOp.getResult());
    } else {
      auto mulOp =
          rewriter.create<C::MulIOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, mulOp.getResult());
    }
    return matchSuccess();
  }
};

class DivOpConversion : public ConversionPattern {
public:
  DivOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::DivOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Type resType = op->getResult(0)->getType();
    if (resType.isF32() || resType.isF64()) {
      auto divOp =
          rewriter.create<C::DivFOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, divOp.getResult());
    } else {
      auto divOp =
          rewriter.create<C::DivISOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, divOp.getResult());
    }
    return matchSuccess();
  }
};

class MaxOpConversion : public ConversionPattern {
public:
  MaxOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::MaxOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Type resType = op->getResult(0)->getType();
    if (resType.isF32() || resType.isF64()) {
      auto addOp =
          rewriter.create<C::MaxFOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, addOp.getResult());
    } else {
      auto addOp =
          rewriter.create<C::MaxIOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, addOp.getResult());
    }
    return matchSuccess();
  }
};

class MinOpConversion : public ConversionPattern {
public:
  MinOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::MinOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Type resType = op->getResult(0)->getType();
    if (resType.isF32() || resType.isF64()) {
      auto addOp =
          rewriter.create<C::MinFOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, addOp.getResult());
    } else {
      auto addOp =
          rewriter.create<C::MinIOp>(op->getLoc(), operands[0], operands[1]);
      rewriter.replaceOp(op, addOp.getResult());
    }
    return matchSuccess();
  }
};

class AndOpConversion : public ConversionPattern {
public:
  AndOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::AndOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<C::AndOp>(op, operands[0], operands[1]);
    return matchSuccess();
  }
};

class OrOpConversion : public ConversionPattern {
public:
  OrOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::OrOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<C::OrOp>(op, operands[0], operands[1]);
    return matchSuccess();
  }
};

class EqualOpConversion : public ConversionPattern {
public:
  EqualOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::EqualOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<C::EqualOp>(op, rewriter.getI1Type(),
                                            operands[0], operands[1]);
    return matchSuccess();
  }
};

class NotEqualOpConversion : public ConversionPattern {
public:
  NotEqualOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::NotEqualOp::getOperationName(), 1, context) {
  }

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<C::NotEqualOp>(op, rewriter.getI1Type(),
                                               operands[0], operands[1]);
    return matchSuccess();
  }
};

class LtOpConversion : public ConversionPattern {
public:
  LtOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::LtOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<C::LtOp>(op, rewriter.getI1Type(), operands[0],
                                         operands[1]);
    return matchSuccess();
  }
};

class GtOpConversion : public ConversionPattern {
public:
  GtOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::GtOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<C::GtOp>(op, rewriter.getI1Type(), operands[0],
                                         operands[1]);
    return matchSuccess();
  }
};

class LeOpConversion : public ConversionPattern {
public:
  LeOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::LeOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<C::LeOp>(op, rewriter.getI1Type(), operands[0],
                                         operands[1]);
    return matchSuccess();
  }
};

class GeOpConversion : public ConversionPattern {
public:
  GeOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::GeOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<C::GeOp>(op, rewriter.getI1Type(), operands[0],
                                         operands[1]);
    return matchSuccess();
  }
};

class NegOpConversion : public ConversionPattern {
public:
  NegOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::NegOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Type resType = op->getResult(0)->getType();
    if (resType.isF32() || resType.isF64()) {
      ConstantOp zero = rewriter.create<ConstantOp>(
          op->getLoc(), rewriter.getF64Type(),
          FloatAttr::get(rewriter.getF64Type(), 0.0));
      auto subOp = rewriter.create<C::SubFOp>(op->getLoc(), zero.getResult(),
                                              operands[0]);
      rewriter.replaceOp(op, subOp.getResult());
    } else {
      ConstantOp zero = rewriter.create<ConstantOp>(
          op->getLoc(), rewriter.getIntegerType(32),
          IntegerAttr::get(rewriter.getIntegerType(32), 0));
      auto subOp = rewriter.create<C::SubIOp>(op->getLoc(), zero.getResult(),
                                              operands[0]);
      rewriter.replaceOp(op, subOp.getResult());
    }
    return matchSuccess();
  }
};

static SmallVector<mlir::Value *, 3> getInductionVars(Operation *op) {
  SmallVector<mlir::Value *, 3> ret(3, nullptr);
  if (!op->getBlock())
    return ret;
  if (auto parent = op->getBlock()->getContainingOp())
    ret = getInductionVars(parent);
  for (auto it = op->getBlock()->begin(); it != op->getBlock()->end(); ++it) {
    if (auto op = dyn_cast<Stencil::InductionVarOp>(*it)) {
      if (op.direction() == "I")
        ret[0] = op.value();
      if (op.direction() == "J")
        ret[1] = op.value();
      if (op.direction() == "K")
        ret[2] = op.value();
    }
  }
  return ret;
}

class WriteOpConversion : public ConversionPattern {
public:
  WriteOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::WriteOp::getOperationName(), 1, context),
        temporaries(temporaries) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Module m = op->getFunction().getModule();
    Function writeFunction;
    if (std::find(std::begin(temporaries), std::end(temporaries),
                  operands[0]) != std::end(temporaries))
      writeFunction = m.getNamedFunction("writeTemp");
    else
      writeFunction = m.getNamedFunction("write");

    auto inductionVars = getInductionVars(op);
    if (!llvm::all_of(inductionVars, [](mlir::Value *v) { return v; }))
      return matchFailure();

    SmallVector<Value *, 5> args;
    for (const auto &val : operands)
      args.push_back(val);
    args.push_back(inductionVars[0]); // i
    args.push_back(inductionVars[1]); // j
    args.push_back(inductionVars[2]); // k
    rewriter.create<CallOp>(op->getLoc(), writeFunction, args);
    rewriter.replaceOp(op, llvm::None);
    return matchSuccess();
  }

private:
  std::vector<Value *> &temporaries;
};

class ReadOpConversion : public ConversionPattern {
public:
  ReadOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::ReadOp::getOperationName(), 1, context),
        temporaries(temporaries) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    SmallVector<Value *, 4> args;
    args.push_back(operands[0]);
    auto inductionVars = getInductionVars(op);
    if (!llvm::all_of(inductionVars, [](mlir::Value *v) { return v; }))
      return matchFailure();

    args.push_back(mapping.lookupOrDefault(inductionVars[0])); // i
    args.push_back(mapping.lookupOrDefault(inductionVars[1])); // j
    args.push_back(mapping.lookupOrDefault(inductionVars[2])); // k
    args.push_back(operands[1]);

    Module m = op->getFunction().getModule();
    Function readFunction;
    if (std::find(std::begin(temporaries), std::end(temporaries),
                  operands[0]) != std::end(temporaries))
      readFunction = m.getNamedFunction("readTemp");
    else
      readFunction = m.getNamedFunction("read");
    CallOp callOp = rewriter.create<CallOp>(op->getLoc(), readFunction, args);
    rewriter.replaceOp(op, callOp.getResult(0));
    return matchSuccess();
  }

private:
  std::vector<Value *> &temporaries;
};

class ContextOpConversion : public ConversionPattern {
public:
  ContextOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::ContextOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    StringRef varName = op->getAttr("name").dyn_cast<StringAttr>().getValue();
    Module m = op->getFunction().getModule();
    Function contextFunction = m.getNamedFunction(varName);
    CallOp callOp =
        rewriter.create<CallOp>(op->getLoc(), contextFunction, operands);
    rewriter.replaceOp(op, callOp.getResult(0));
    return matchSuccess();
  }
};

class TempOpConversion : public ConversionPattern {
public:
  TempOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::TempOp::getOperationName(), 1, context),
        temporaries(temporaries) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Module m = op->getFunction().getModule();
    Function tempFunction = m.getNamedFunction("temp");
    CallOp callOp =
        rewriter.create<CallOp>(op->getLoc(), tempFunction, operands);
    temporaries.push_back(callOp.getResult(0));
    rewriter.replaceOp(op, callOp.getResult(0));
    return matchSuccess();
  }

private:
  std::vector<Value *> &temporaries;
};

class AffineForOpConversion : public ConversionPattern {
public:
  AffineForOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(AffineForOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    AffineForOp affineForOp = dyn_cast<AffineForOp>(op);
    AffineMap lowerBoundMap = affineForOp.getLowerBoundMap();
    SmallVector<Value *, 8> lowerBoundOperands(
        affineForOp.getLowerBoundOperands());
    AffineMap upperBoundMap = affineForOp.getUpperBoundMap();
    SmallVector<Value *, 8> upperBoundOperands(
        affineForOp.getUpperBoundOperands());
    auto expandLower = expandAffineMap(&rewriter, op->getLoc(), lowerBoundMap,
                                       lowerBoundOperands);
    if (!expandLower)
      return matchFailure();
    auto expandUpper = expandAffineMap(&rewriter, op->getLoc(), upperBoundMap,
                                       upperBoundOperands);
    if (!expandUpper)
      return matchFailure();
    C::ForOp forOp = rewriter.create<C::ForOp>(
        op->getLoc(), (*expandLower).back(), (*expandUpper).back());
    forOp.getBody().getBlocks().clear();
    forOp.getBody().takeBody(op->getRegion(0));
    rewriter.replaceOp(op, llvm::None);
    return matchSuccess();
  }
};

class AffineApplyOpConversion : public ConversionPattern {
public:
  AffineApplyOpConversion(MLIRContext *context,
                          std::vector<Value *> &temporaries)
      : ConversionPattern(AffineApplyOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    AffineApplyOp affineApplyOp = cast<AffineApplyOp>(op);
    auto maybeExpandedMap = expandAffineMap(
        &rewriter, op->getLoc(), affineApplyOp.getAffineMap(), operands);
    if (!maybeExpandedMap)
      return matchFailure();
    rewriter.replaceOp(op, *maybeExpandedMap);
    return matchSuccess();
  }
};

class SetGlobalOpConversion : public ConversionPattern {
public:
  SetGlobalOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::SetGlobalOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    StringRef varName = op->getAttr("name").dyn_cast<StringAttr>().getValue();
    Module m = op->getFunction().getModule();
    Function setFunction = Function::create(
        UnknownLoc::get(m.getContext()), std::string("set_") + varName.str(),
        FunctionType::get({operands[0]->getType()}, {}, m.getContext()));
    m.insert(m.begin(), setFunction);
    CallOp callOp =
        rewriter.create<CallOp>(op->getLoc(), setFunction, operands);
    rewriter.replaceOp(op, llvm::None);
    return matchSuccess();
  }
};

class GetGlobalOpConversion : public ConversionPattern {
public:
  GetGlobalOpConversion(MLIRContext *context, std::vector<Value *> &temporaries)
      : ConversionPattern(Stencil::GetGlobalOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    StringRef varName = op->getAttr("name").dyn_cast<StringAttr>().getValue();
    Module m = op->getFunction().getModule();
    Function setFunction = Function::create(
        UnknownLoc::get(m.getContext()), std::string("get_") + varName.str(),
        FunctionType::get({}, {op->getResult(0)->getType()}, m.getContext()));
    m.insert(m.begin(), setFunction);
    CallOp callOp =
        rewriter.create<CallOp>(op->getLoc(), setFunction, operands);
    rewriter.replaceOp(op, callOp.getResult(0));
    return matchSuccess();
  }
};

class CTypeConverter : public TypeConverter {
public:
  explicit CTypeConverter(MLIRContext *context) : context(context) {}

  MLIRContext *getContext() { return context; }

protected:
  Type convertType(Type t) {
    if (t.isa<Stencil::StencilFieldType>())
      return C::VoidPtrType::get(context);
    else
      return t;
  }

private:
  MLIRContext *context;
};

void populateStencilToCConversionPatterns(
    CTypeConverter &converter, mlir::OwningRewritePatternList &patterns,
    std::vector<Value *> &temporaries) {
  RewriteListBuilder<
      AddOpConversion, SubOpConversion, MulOpConversion, DivOpConversion,
      AndOpConversion, OrOpConversion, MaxOpConversion, MinOpConversion,
      EqualOpConversion, NotEqualOpConversion, LtOpConversion, GtOpConversion,
      LeOpConversion, GeOpConversion, NegOpConversion, TempOpConversion,
      WriteOpConversion, ReadOpConversion, ContextOpConversion,
      AffineApplyOpConversion, AffineForOpConversion, SetGlobalOpConversion,
      GetGlobalOpConversion>::build(patterns, converter.getContext(),
                                    temporaries);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Lowering pass

class StencilToCLoweringPass : public ModulePass<StencilToCLoweringPass> {
public:
  void runOnModule() override {
    Module m = getModule();

    registerExternalFunctions();

    CTypeConverter cConverter(&getContext());
    OwningRewritePatternList cPatterns;
    populateStencilToCConversionPatterns(cConverter, cPatterns, m_temporaries);

    ConversionTarget stencilCTarget(getContext());
    stencilCTarget.addLegalDialect<CDialect, StandardOpsDialect>();
    stencilCTarget
        .addLegalOp<AffineTerminatorOp, // FIXME: Add CTerminatorOp
                    Stencil::DeclareGlobalOp, Stencil::InductionVarOp,
                              Stencil::IfOp>();
    if (failed(applyConversionPatterns(m, stencilCTarget, cConverter,
                                       std::move(cPatterns))))
      signalPassFailure();
  }

private:
  void registerExternalFunctions() {
    Module m = getModule();
    MLIRContext *context = &getContext();
    FloatType f64 = FloatType::getF64(context);
    C::VoidPtrType voidPtr = C::VoidPtrType::get(context);
    auto indexType = IndexType::get(context);
    Stencil::StencilOffsetType offset =
        Stencil::StencilOffsetType::get(context);

    auto writeType = FunctionType::get(
        {voidPtr, f64, indexType, indexType, indexType}, {}, context);
    Function writeFunction =
        Function::create(UnknownLoc::get(context), "write", writeType);
    m.insert(m.begin(), writeFunction);
    Function writeTempFunction =
        Function::create(UnknownLoc::get(context), "writeTemp", writeType);
    m.insert(m.begin(), writeTempFunction);

    auto readType = FunctionType::get(
        {voidPtr, indexType, indexType, indexType, offset}, {f64}, context);
    Function readFunction =
        Function::create(UnknownLoc::get(context), "read", readType);
    m.insert(m.begin(), readFunction);
    Function readTempFunction =
        Function::create(UnknownLoc::get(context), "readTemp", readType);
    m.insert(m.begin(), readTempFunction);

    Function tempFunction =
        Function::create(UnknownLoc::get(context), "temp",
                         FunctionType::get({}, {voidPtr}, context));
    m.insert(m.begin(), tempFunction);

    registerContextFunction("kstart", indexType);
    registerContextFunction("kend", indexType);
    registerContextFunction("istart", indexType);
    registerContextFunction("iend", indexType);
    registerContextFunction("jstart", indexType);
    registerContextFunction("jend", indexType);
  }

  void registerContextFunction(StringRef varName, Type type) {
    Module m = getModule();
    Function contextFunction =
        Function::create(UnknownLoc::get(m.getContext()), varName,
                         FunctionType::get({}, {type}, m.getContext()));
    m.insert(m.begin(), contextFunction);
  }

private:
  std::vector<Value *> m_temporaries;
};

static PassRegistration<StencilToCLoweringPass> pass("stencil-to-c",
                                                     "Convert stencils to C");

} // namespace
