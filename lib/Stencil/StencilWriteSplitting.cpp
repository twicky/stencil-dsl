#include "mlir/AffineOps/AffineOps.h"
#include "mlir/Analysis/Dominance.h"
#include "mlir/C/COps.h"
#include "mlir/C/CTypes.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/AffineExprVisitor.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/BlockAndValueMapping.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/OperationSupport.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Linalg/IR/LinalgTypes.h"
#include "mlir/Pass/Pass.h"
#include "mlir/StandardOps/Ops.h"
#include "mlir/Stencil/StencilOps.h"
#include "mlir/Support/Functional.h"
#include "mlir/Support/LLVM.h"
#include "mlir/Support/STLExtras.h"
#include "mlir/Transforms/DialectConversion.h"

#include <queue>

using namespace mlir;

namespace {

class VerticalRegionOpConversion : public ConversionPattern {
public:
  VerticalRegionOpConversion(MLIRContext *context)
      : ConversionPattern(Stencil::VerticalRegionOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    std::queue<Block *> toProcess;
    std::vector<Block *> resultingBlocks;
    toProcess.push(&op->getRegion(0).front());
    bool backward = bool(op->getAttr("backward"));
    while (!toProcess.empty()) {
      Block *block = toProcess.front();
      toProcess.pop();
      if (block->empty())
        break;
      Operation *lastOp = &block->front();
      bool splitHappened = false;
      for (Block::iterator op = block->begin();
           op != block->end() && !splitHappened; ++op) {
        if (isa<Stencil::WriteOp>(*lastOp) && !op->isKnownTerminator()) {
          Block *headBlock = new Block();
          Operation *newVerticalRegion =
              rewriter.create<Stencil::VerticalRegionOp>(
                  op->getLoc(), operands[0], operands[1]);
          if (backward)
            newVerticalRegion->setAttr("backward",
                                       UnitAttr::get(op->getContext()));
          newVerticalRegion->getRegion(0).getBlocks().clear();
          newVerticalRegion->getRegion(0).push_back(headBlock);
          OpBuilder::InsertPoint savePoint = rewriter.saveInsertionPoint();
          headBlock->getOperations().splice(
              headBlock->end(), block->getOperations(), block->begin(), op);
          rewriter.setInsertionPointToEnd(headBlock);
          rewriter.create<Stencil::TerminatorOp>(op->getLoc());
          rewriter.restoreInsertionPoint(savePoint);
          toProcess.push(block);
          splitHappened = true;
        }
        lastOp = &(*op);
      }

      // Last vertical region
      if (!splitHappened) {
        rewriter.clone(*op, mapping);
      }
    }

    rewriter.replaceOp(op, llvm::None);
    return matchSuccess();
  }
};

class SplittingTypeConverter : public TypeConverter {
public:
  explicit SplittingTypeConverter(MLIRContext *context) : context(context) {}

  MLIRContext *getContext() { return context; }

private:
  MLIRContext *context;
};

void populateStencilToAffineConversionPatterns(
    SplittingTypeConverter &converter, OwningRewritePatternList &patterns) {
  RewriteListBuilder<VerticalRegionOpConversion>::build(patterns,
                                                        converter.getContext());
}

class SplittingTarget : public ConversionTarget {
public:
  using ConversionTarget::ConversionTarget;

  bool isDynamicallyLegal(Operation *op) const override {
    if (isa<Stencil::VerticalRegionOp>(*op)) {
      return isSplittedOnWrites(op);
    }
    return true;
  }

private:
  bool isSplittedOnWrites(Operation *verticalRegion) const {
    Block &body = verticalRegion->getRegion(0).front();
    Operation *lastOp = &body.front();
    for (Operation &op : body.getOperations()) {
      if (isa<Stencil::WriteOp>(*lastOp) && !op.isKnownTerminator())
        return false;
      lastOp = &op;
    }
    return true;
  }
};

class StencilLoopSplittingPass : public ModulePass<StencilLoopSplittingPass> {
public:
  void runOnModule() override {
    Module m = getModule();

    SplittingTypeConverter converter(&getContext());
    OwningRewritePatternList patterns;
    populateStencilToAffineConversionPatterns(converter, patterns);

    SplittingTarget target(getContext());
    target.addDynamicallyLegalOp<Stencil::VerticalRegionOp>();
    target.addLegalOp<
        Stencil::AddOp, Stencil::SubOp, Stencil::MulOp, Stencil::DivOp,
        Stencil::MinOp, Stencil::MaxOp, Stencil::AndOp, Stencil::OrOp,
        Stencil::EqualOp, Stencil::NotEqualOp, Stencil::LtOp, Stencil::GtOp,
        Stencil::LeOp, Stencil::GeOp, Stencil::NegOp, Stencil::ReadOp,
        Stencil::WriteOp, Stencil::TerminatorOp, Stencil::TempOp,
        Stencil::ContextOp, Stencil::GetGlobalOp, Stencil::SetGlobalOp,
        Stencil::DeclareGlobalOp, Stencil::IfOp>();
    target.addLegalOp<ReturnOp>();
    if (failed(
            applyConversionPatterns(m, target, converter, std::move(patterns))))
      signalPassFailure();

    for (Function f : m) {
      DominanceInfo dominance(f);
      for (Block &b : f) {
        for (Operation &op : b) {
          if (isa<Stencil::VerticalRegionOp>(op)) {
            Block &verticalRegionBody = op.getRegion(0).front();
            for (auto vop = verticalRegionBody.rbegin();
                 vop != verticalRegionBody.rend(); ++vop) {
              for (unsigned int i = 0; i < (*vop).getNumOperands(); ++i) {
                Value *operand = (*vop).getOperand(i);
                Operation *definingOp = operand->getDefiningOp();
                if (definingOp && !dominance.dominates(definingOp, &(*vop)))
                  cloneAndReplace(definingOp, dominance, &verticalRegionBody);
              }
            }
          }
        }
      }
    }
  }

private:
  void cloneAndReplace(Operation *op, DominanceInfo &dominance, Block *block) {
    OpBuilder opBuilder(block);
    opBuilder.setInsertionPointToStart(block);
    Operation *cloned = opBuilder.clone(*op);
    Value *result = cloned->getResult(0);
    for (auto replace = block->begin(); replace != block->end(); ++replace) {
      (*replace).replaceUsesOfWith(op->getResult(0), result);
    }
    for (unsigned int i = 0; i < cloned->getNumOperands(); ++i) {
      Value *operand = cloned->getOperand(i);
      Operation *definingOp = operand->getDefiningOp();
      if (definingOp && !dominance.properlyDominates(definingOp, cloned))
        cloneAndReplace(definingOp, dominance, block);
    }
  }
};

static PassRegistration<StencilLoopSplittingPass>
    pass("stencil-split-loops",
         "Split loops to have only a single write operation per iteration");

} // namespace
