#include "mlir/AffineOps/AffineOps.h"
#include "mlir/C/COps.h"
#include "mlir/C/CTypes.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/AffineExprVisitor.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/BlockAndValueMapping.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Linalg/IR/LinalgTypes.h"
#include "mlir/Pass/Pass.h"
#include "mlir/StandardOps/Ops.h"
#include "mlir/Stencil/StencilOps.h"
#include "mlir/Support/Functional.h"
#include "mlir/Support/LLVM.h"
#include "mlir/Support/STLExtras.h"
#include "mlir/Transforms/DialectConversion.h"

#include <map>

using namespace mlir;

namespace {

class LambdaOpConversion : public ConversionPattern {
public:
  LambdaOpConversion(MLIRContext *context)
      : ConversionPattern(Stencil::LambdaOp::getOperationName(), 1, context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Stencil::LambdaOp lambdaOp = cast<Stencil::LambdaOp>(*op);
    Type fieldType = op->getResult(0)->getType();
    OpBuilder::InsertPoint insertPoint = rewriter.saveInsertionPoint();
    rewriter.setInsertionPointToStart(&op->getFunction().front());
    Stencil::TempOp tempOp =
        rewriter.create<Stencil::TempOp>(op->getLoc(), fieldType);
    rewriter.restoreInsertionPoint(insertPoint);

    Module m = op->getFunction().getModule();
    Function callee = m.getNamedFunction(lambdaOp.callee());

    std::map<Value *, Value *> mappedValues;
    unsigned i = 0;
    for (Value *argument : callee.getArguments()) {
      mappedValues.insert({argument, op->getOperand(i++)});
    }

    Block &calleeBody = callee.getBody().front();
    for (Operation &calleeOp : calleeBody) {
      if (!isa<ReturnOp>(calleeOp)) {
        Operation *clonedOperation = rewriter.clone(calleeOp, mapping);
        if (isa<Stencil::TempOp>(*clonedOperation))
          clonedOperation->moveBefore(&op->getFunction().front().front());
        else {
          for (auto &mapping : mappedValues) {
            clonedOperation->replaceUsesOfWith(mapping.first, mapping.second);
          }
        }
      }
    }

    if (callee.getType().getNumResults() == 1) {
      ReturnOp returnOp = cast<ReturnOp>(calleeBody.getTerminator());
      Value *returnValue = returnOp.getOperand(0);
      rewriter.create<Stencil::WriteOp>(op->getLoc(), tempOp.getResult(),
                                        mapping.lookupOrDefault(returnValue));
    } else if (callee.getType().getNumResults() != 0)
      return matchFailure();

    rewriter.replaceOp(op, tempOp.getResult());
    return matchSuccess();
  }
};

class InlineTypeConverter : public TypeConverter {
public:
  explicit InlineTypeConverter(MLIRContext *context) : context(context) {}

  MLIRContext *getContext() { return context; }

private:
  MLIRContext *context;
};

void populateStencilToAffineConversionPatterns(
    InlineTypeConverter &converter, OwningRewritePatternList &patterns) {
  RewriteListBuilder<LambdaOpConversion>::build(patterns,
                                                converter.getContext());
}

class StencilVerticalRegionToLoopsPass
    : public ModulePass<StencilVerticalRegionToLoopsPass> {
public:
  void runOnModule() override {
    Module m = getModule();

    InlineTypeConverter typeConverter(&getContext());
    OwningRewritePatternList inlinePatterns;
    populateStencilToAffineConversionPatterns(typeConverter, inlinePatterns);

    ConversionTarget stencilAffineTarget(getContext());
    stencilAffineTarget
        .addLegalOp<AffineForOp, AffineApplyOp, AffineTerminatorOp>();
    stencilAffineTarget.addLegalOp<
        Stencil::AddOp, Stencil::SubOp, Stencil::MulOp, Stencil::DivOp,
        Stencil::MinOp, Stencil::MaxOp, Stencil::AndOp, Stencil::OrOp,
        Stencil::EqualOp, Stencil::NotEqualOp, Stencil::LtOp, Stencil::GtOp,
        Stencil::LeOp, Stencil::GeOp, Stencil::NegOp, Stencil::ReadOp,
        Stencil::WriteOp, Stencil::TempOp, Stencil::ContextOp,
        Stencil::GetGlobalOp, Stencil::SetGlobalOp, Stencil::DeclareGlobalOp,
        Stencil::ConstantOffsetOp, Stencil::IfOp>();
    stencilAffineTarget.addLegalOp<ReturnOp>();
    if (failed(applyConversionPatterns(m, stencilAffineTarget, typeConverter,
                                       std::move(inlinePatterns))))
      signalPassFailure();

    ModuleManager moduleManager(m);
    std::vector<Function> stencilFunctions;
    for (Function f : m.getFunctions()) {
      if (f.getAttr("stencil.function"))
        stencilFunctions.push_back(f);
    }

    for (Function f : stencilFunctions) {
      moduleManager.erase(f);
    }
  }
};

static PassRegistration<StencilVerticalRegionToLoopsPass>
    pass("stencil-inline-lambdas",
         "Inline stencil lambda operations to only use reads and writes");

} // namespace
