//===- Dialect.cpp - Implementation of the linalg dialect and types -------===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements the Stencil dialect types and dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/Stencil/StencilTypes.h"
#include "mlir/IR/Dialect.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Stencil/StencilOps.h"
#include "mlir/Support/LLVM.h"
#include "llvm/Support/raw_ostream.h"

using namespace mlir;

namespace mlir {
namespace Stencil {
namespace detail {

struct StencilFieldStorage : public ::mlir::TypeStorage {
  explicit StencilFieldStorage(Type elemType, bool i, bool j, bool k)
      : TypeStorage(3), elementsType(elemType) {
    directions[0] = i;
    directions[1] = j;
    directions[2] = k;
  }

  using KeyTy = std::tuple<Type, int, int, int>;
  bool operator==(const KeyTy &key) const {
    return key ==
           KeyTy(elementsType, directions[0], directions[1], directions[2]);
  }

  static StencilFieldStorage *construct(TypeStorageAllocator &allocator,
                                        KeyTy key) {
    return new (allocator.allocate<StencilFieldStorage>()) StencilFieldStorage(
        std::get<0>(key), std::get<1>(key), std::get<2>(key), std::get<3>(key));
  }

  ArrayRef<bool> getDirections() const {
    return ArrayRef<bool>(directions, getSubclassData());
  }

  Type elementsType;
  bool directions[3];
};

} // namespace detail
} // namespace Stencil
} // namespace mlir

Stencil::StencilFieldType Stencil::StencilFieldType::get(MLIRContext *context,
                                                         Type elementsType,
                                                         bool i, bool j,
                                                         bool k) {
  assert(i && j && k && "Unsupported field type");
  return Base::get(context, StencilTypes::StencilField, elementsType, i, j, k);
}

Type Stencil::StencilFieldType::getElementType() const {
  return getImpl()->elementsType;
}

ArrayRef<bool> Stencil::StencilFieldType::getDirections() const {
  return getImpl()->getDirections();
}

bool Stencil::StencilFieldType::iDirection() const {
  return getImpl()->directions[0];
}

bool Stencil::StencilFieldType::jDirection() const {
  return getImpl()->directions[1];
}

bool Stencil::StencilFieldType::kDirection() const {
  return getImpl()->directions[2];
}

Stencil::StencilOffsetType
Stencil::StencilOffsetType::get(MLIRContext *context) {
  return Base::get(context, StencilTypes::StencilOffset);
}

Stencil::StencilDialect::StencilDialect(MLIRContext *context)
    : Dialect(getDialectNamespace(), context) {
  addTypes<StencilFieldType, StencilOffsetType>();
  addOperations<
#define GET_OP_LIST
#include "mlir/Stencil/StencilOps.cpp.inc"
      >();
  allowUnknownOperations();
}

static void print(Stencil::StencilFieldType ft, raw_ostream &os) {
  os << "field:" << ft.getElementType();
}
static void print(Stencil::StencilOffsetType ft, raw_ostream &os) {
  os << "offset";
}

Type Stencil::StencilDialect::parseType(StringRef spec, Location loc) const {
  MLIRContext *context = getContext();
  if (spec.startswith("field:")) {
    SmallVector<StringRef, 2> ss;
    spec.split(ss, ':');
    if (ss.size() == 2) {
      StringRef typeStr = ss[1];
      if (typeStr == "f32")
        return Stencil::StencilFieldType::get(
            context, FloatType::getF32(context), true, true, true);
      else if (typeStr == "f64")
        return Stencil::StencilFieldType::get(
            context, FloatType::getF64(context), true, true, true);
      else if (typeStr == "i64")
        return Stencil::StencilFieldType::get(
            context, IntegerType::get(64, context), true, true, true);
    }
  } else if (spec == "offset") {
    return Stencil::StencilOffsetType::get(context);
  }
  return (emitError(loc, "unknown Stencil type: " + spec), Type());
}

void Stencil::StencilDialect::printType(Type type, raw_ostream &os) const {
  switch (type.getKind()) {
  default:
    llvm_unreachable("Unhandled Stencil type");
  case StencilTypes::StencilField:
    print(type.cast<Stencil::StencilFieldType>(), os);
    break;
  case StencilTypes::StencilOffset:
    print(type.cast<Stencil::StencilOffsetType>(), os);
    break;
  }
}
