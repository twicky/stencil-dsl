//===- StencilOps.cpp - Implementation of the Stencil operations ----------===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements a the Linalg operations.
//
//===----------------------------------------------------------------------===//

#include "mlir/Stencil/StencilOps.h"
#include "mlir/AffineOps/AffineOps.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/BlockAndValueMapping.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Linalg/IR/LinalgTypes.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Support/LLVM.h"
#include "mlir/Support/STLExtras.h"
#include "mlir/Transforms/DialectConversion.h"

#include <iostream>

using namespace mlir;

static void ensureTerminator(Region &region, Builder &builder, Location loc) {
  if (region.empty())
    region.push_back(new Block());
  Block &block = region.back();
  if (!block.empty() && block.back().isKnownTerminator())
    return;

  OperationState terminatorState(loc,
                                 Stencil::TerminatorOp::getOperationName());
  Stencil::TerminatorOp::build(&builder, &terminatorState);
  block.push_back(Operation::create(terminatorState));
}

static ParseResult parseTerminator(OpAsmParser *parser,
                                   OperationState *result) {
  return success();
}

static void printTerminator(OpAsmPrinter *p, Stencil::TerminatorOp &op) {
  return;
}

static ParseResult parseStencilBinaryOp(OpAsmParser *parser,
                                        OperationState *result) {
  SmallVector<OpAsmParser::OperandType, 2> operands;
  Type type;
  return failure(
      parser->parseOperandList(operands, 2, OpAsmParser::Delimiter::Paren) ||
      parser->parseColonType(type) ||
      parser->resolveOperands(operands, type, result->operands) ||
      parser->addTypeToList(type, result->types));
}

static void printStencilBinaryOp(OpAsmPrinter *p, Operation *op) {
  const auto *abstract = op->getAbstractOperation();
  assert(abstract && "pretty-printing an unregistered operation");

  *p << op->getName().getStringRef() << "(" << *op->getOperand(0) << ", "
     << *op->getOperand(1) << ")";
  p->printOptionalAttrDict(op->getAttrs());
  *p << " : " << op->getResult(0)->getType();
}

static ParseResult parseStencilComparisonOp(OpAsmParser *parser,
                                            OperationState *result) {
  SmallVector<OpAsmParser::OperandType, 2> operands;
  Type type;
  return failure(
      parser->parseOperandList(operands, 2, OpAsmParser::Delimiter::Paren) ||
      parser->parseColonType(type) ||
      parser->resolveOperands(operands, type, result->operands) ||
      parser->addTypeToList(parser->getBuilder().getI1Type(), result->types));
}

static void printStencilComparisonOp(OpAsmPrinter *p, Operation *op) {
  const auto *abstract = op->getAbstractOperation();
  assert(abstract && "pretty-printing an unregistered operation");

  *p << op->getName().getStringRef() << "(" << *op->getOperand(0) << ", "
     << *op->getOperand(1) << ")";
  p->printOptionalAttrDict(op->getAttrs());
  *p << " : " << op->getOperand(0)->getType();
}

static ParseResult parseNeg(OpAsmParser *parser, OperationState *result) {
  SmallVector<OpAsmParser::OperandType, 1> operand;
  Type type;
  return failure(
      parser->parseOperandList(operand, 1, OpAsmParser::Delimiter::Paren) ||
      parser->parseColonType(type) ||
      parser->resolveOperands(operand, type, result->operands) ||
      parser->addTypeToList(type, result->types));
}

static void printNeg(OpAsmPrinter *p, Stencil::NegOp &op) {
  *p << op.getOperationName() << "(" << *op.value() << ") : " << op.getType();
}

static ParseResult parseRead(OpAsmParser *parser, OperationState *result) {
  SmallVector<OpAsmParser::OperandType, 2> operands;
  Type type;
  if (parser->parseOperandList(operands, 2, OpAsmParser::Delimiter::Paren) ||
      parser->parseColonType(type))
    return failure();
  return failure(parser->resolveOperands(operands[0],
                                         Stencil::StencilFieldType::get(
                                             parser->getBuilder().getContext(),
                                             type, true, true, true),
                                         result->operands) ||
                 parser->resolveOperands(operands[1],
                                         Stencil::StencilOffsetType::get(
                                             parser->getBuilder().getContext()),
                                         result->operands) ||
                 parser->addTypeToList(type, result->types));
}

static void printRead(OpAsmPrinter *p, Stencil::ReadOp &op) {
  *p << op.getOperationName() << "(" << *op.field() << ", " << *op.offset()
     << ") : " << op.getType();
}

static ParseResult parseTemp(OpAsmParser *parser, OperationState *result) {
  Type type;
  if (parser->parseColonType(type))
    return failure();
  Stencil::StencilFieldType tempType;
  if (!(tempType = type.dyn_cast<Stencil::StencilFieldType>()))
    return failure();
  return parser->addTypeToList(tempType, result->types);
}

static void printTemp(OpAsmPrinter *p, Stencil::TempOp &op) {
  *p << op.getOperationName() << " : " << op.getType();
}

static ParseResult parseVerticalRegion(OpAsmParser *parser,
                                       OperationState *result) {
  SmallVector<OpAsmParser::OperandType, 2> operands;
  Region *verticalRegion = result->addRegion();
  if (parser->parseOperandList(operands, 2, OpAsmParser::Delimiter::Paren))
    return failure();
  if (!parser->parseOptionalKeyword("backward"))
    result->addAttribute("backward", UnitAttr::get(result->context));
  if (parser->parseRegion(*verticalRegion, {}, {}))
    return failure();
  ensureTerminator(*verticalRegion, parser->getBuilder(), result->location);
  return parser->resolveOperands(operands, parser->getBuilder().getIndexType(),
                                 result->operands);
}

static void printVerticalRegion(OpAsmPrinter *p,
                                Stencil::VerticalRegionOp &op) {
  *p << op.getOperationName() << "(" << *op.start() << ", " << *op.end()
     << (op.getOperation()->getAttr("backward") ? ") backward" : ")");
  p->printRegion(op.getOperation()->getRegion(0), false, false);
}

static ParseResult parseWrite(OpAsmParser *parser, OperationState *result) {
  SmallVector<OpAsmParser::OperandType, 2> operands;
  Type type;
  if (parser->parseOperandList(operands, 2, OpAsmParser::Delimiter::Paren) ||
      parser->parseColonType(type))
    return failure();
  return failure(parser->resolveOperands(operands[0],
                                         Stencil::StencilFieldType::get(
                                             parser->getBuilder().getContext(),
                                             type, true, true, true),
                                         result->operands) ||
                 parser->resolveOperands(operands[1], type, result->operands));
}

static void printWrite(OpAsmPrinter *p, Stencil::WriteOp &op) {
  *p << op.getOperationName() << "(" << *op.field() << ", " << *op.value()
     << ") : " << op.value()->getType();
}

static ParseResult parseContext(OpAsmParser *parser, OperationState *result) {
  StringAttr name;
  return failure(parser->parseAttribute(name, "name", result->attributes) ||
                 parser->addTypeToList(name.getType(), result->types));
}

static void printContext(OpAsmPrinter *p, Stencil::ContextOp &op) {
  *p << op.getOperationName() << " \"" << op.name() << "\" : " << op.getType();
}

static ParseResult parseGetGlobal(OpAsmParser *parser, OperationState *result) {
  StringAttr name;
  Type type;
  return failure(parser->parseAttribute(name, "name", result->attributes) ||
                 parser->addTypeToList(name.getType(), result->types));
}

static void printGetGlobal(OpAsmPrinter *p, Stencil::GetGlobalOp &op) {
  *p << op.getOperationName() << " \"" << op.name() << "\" : " << op.getType();
}

static ParseResult parseSetGlobal(OpAsmParser *parser, OperationState *result) {
  StringAttr name;
  Type type;
  OpAsmParser::OperandType value;
  return failure(parser->parseAttribute(name, "name", result->attributes) ||
                 parser->parseOperand(value) || parser->parseColonType(type) ||
                 parser->resolveOperands(value, type, result->operands));
}

static void printSetGlobal(OpAsmPrinter *p, Stencil::SetGlobalOp &op) {
  *p << op.getOperationName() << " \"" << op.name() << "\" " << *op.value()
     << " : " << op.getOperand()->getType();
}

static ParseResult parseDeclareGlobal(OpAsmParser *parser,
                                      OperationState *result) {
  StringAttr name;
  TypeAttr type;
  return failure(parser->parseAttribute(name, "name", result->attributes) ||
                 parser->parseAttribute(type, "type", result->attributes));
}

static void printDeclareGlobal(OpAsmPrinter *p, Stencil::DeclareGlobalOp &op) {
  *p << op.getOperationName() << " \"" << op.name() << "\" " << op.type();
}

static ParseResult parseConstantOffset(OpAsmParser *parser,
                                       OperationState *result) {
  IntegerAttr i, j, k;
  return failure(parser->parseAttribute(i, "i", result->attributes) ||
                 parser->parseAttribute(j, "j", result->attributes) ||
                 parser->parseAttribute(k, "k", result->attributes) ||
                 parser->addTypeToList(Stencil::StencilOffsetType::get(
                                           parser->getBuilder().getContext()),
                                       result->types));
}

static void printConstantOffset(OpAsmPrinter *p,
                                Stencil::ConstantOffsetOp &op) {
  *p << op.getOperationName() << " " << op.i() << " " << op.j() << " "
     << op.k();
}

static ParseResult parseOffsetAdd(OpAsmParser *parser, OperationState *result) {
  Type intervalType =
      Stencil::StencilOffsetType::get(parser->getBuilder().getContext());
  IntegerAttr offsetAttr;
  OpAsmParser::OperandType intervalOperand;
  if (parser->parseOperand(intervalOperand) || parser->parseComma() ||
      parser->parseAttribute(offsetAttr, "offset", result->attributes) ||
      parser->resolveOperand(intervalOperand, intervalType, result->operands) ||
      parser->addTypeToList(intervalType, result->types))
    return failure();

  return parser->parseOptionalAttributeDict(result->attributes);
}

static void printOffsetAdd(OpAsmPrinter *p, Stencil::OffsetAddOp &op) {
  *p << Stencil::OffsetAddOp::getOperationName() << ' ' << *op.getOperand()
     << ", " << op.offset();
  p->printOptionalAttrDict(op.getAttrs(), {"offset"});
}

static ParseResult parseOffsetNeg(OpAsmParser *parser, OperationState *result) {
  Type intervalType =
      Stencil::StencilOffsetType::get(parser->getBuilder().getContext());
  IntegerAttr offsetAttr;
  OpAsmParser::OperandType intervalOperand;
  if (parser->parseOperand(intervalOperand) ||
      parser->resolveOperand(intervalOperand, intervalType, result->operands) ||
      parser->addTypeToList(intervalType, result->types))
    return failure();

  return parser->parseOptionalAttributeDict(result->attributes);
}

static void printOffsetNeg(OpAsmPrinter *p, Stencil::OffsetNegOp &op) {
  *p << Stencil::OffsetAddOp::getOperationName() << ' ' << *op.getOperand();
  p->printOptionalAttrDict(op.getAttrs());
}

static ParseResult parseLambda(OpAsmParser *parser, OperationState *result) {
  FunctionAttr calleeAttr;
  FunctionType calleeType;
  SmallVector<OpAsmParser::OperandType, 4> operands;
  parser->addTypesToList(
      Stencil::StencilFieldType::get(result->getContext(),
                                     FloatType::getF64(result->getContext()),
                                     true, true, true),
      result->types);
  auto calleeLoc = parser->getNameLoc();
  if (parser->parseAttribute(calleeAttr, "callee", result->attributes) ||
      parser->parseOperandList(operands, OpAsmParser::Delimiter::Paren) ||
      parser->parseOptionalAttributeDict(result->attributes) ||
      parser->parseColonType(calleeType) ||
      parser->resolveOperands(operands, calleeType.getInputs(), calleeLoc,
                              result->operands))
    return failure();

  result->addAttribute("callee_type",
                       parser->getBuilder().getTypeAttr(calleeType));

  return success();
}

static void printLambda(OpAsmPrinter *p, Stencil::LambdaOp &op) {
  *p << "stencil.lambda " << op.getAttr("callee") << "(";
  p->printOperands(op.getOperands());
  *p << ")";
  p->printOptionalAttrDict(op.getAttrs(), {"callee", "callee_type"});
  *p << " : " << op.getAttr("callee_type");
}

static ParseResult parseIf(OpAsmParser *parser, OperationState *result) {
  result->regions.reserve(2);
  Region *thenRegion = result->addRegion();
  Region *elseRegion = result->addRegion();

  Builder &builder = parser->getBuilder();
  OpAsmParser::OperandType condition;
  Type boolType = builder.getI1Type();
  if (parser->parseOperand(condition) ||
      parser->resolveOperands(condition, boolType, result->operands)) {
    return failure();
  }

  if (parser->parseRegion(*thenRegion, {}, {})) {
    return failure();
  }
  ensureTerminator(*thenRegion, builder, result->location);

  if (succeeded(parser->parseOptionalKeyword("else"))) {
    if (parser->parseRegion(*elseRegion, {}, {})) {
      return failure();
    }
    ensureTerminator(*elseRegion, builder, result->location);
  }

  return parser->parseOptionalAttributeDict(result->attributes);
}

static void printIf(OpAsmPrinter *printer, Stencil::IfOp ifOp) {
  *printer << Stencil::IfOp::getOperationName() << ' ' << *ifOp.condition();
  printer->printRegion(ifOp.thenRegion(), false, false);

  Region &elseRegion = ifOp.elseRegion();
  if (!elseRegion.empty()) {
    *printer << " else";
    printer->printRegion(elseRegion, false, false);
  }

  printer->printOptionalAttrDict(ifOp.getAttrs());
}

static ParseResult parseInductionVarOp(OpAsmParser *parser,
                                       OperationState *result) {
  StringAttr direction;
  OpAsmParser::OperandType value;
  Type intType;

  if (parser->parseAttribute(direction, "direction", result->attributes) ||
      parser->parseOperand(value) || parser->parseColonType(intType) ||
      parser->resolveOperand(value, intType, result->operands))
    return failure();
  auto dirStr = direction.getValue();
  if (dirStr != "I" && dirStr != "J" && dirStr != "K")
    return failure();
  return success();
}

static void printInductionVarOp(OpAsmPrinter *p, Stencil::InductionVarOp &op) {
  *p << op.getOperationName() << " \"" << op.direction() << "\" " << *op.value()
     << " : " << op.getOperand()->getType();
}

namespace mlir {
namespace Stencil {

#define GET_OP_CLASSES
#include "mlir/Stencil/StencilOps.cpp.inc"

} // namespace Stencil
} // namespace mlir
