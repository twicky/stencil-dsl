#include "mlir/AffineOps/AffineOps.h"
#include "mlir/C/COps.h"
#include "mlir/C/CTypes.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/AffineExprVisitor.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/BlockAndValueMapping.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Linalg/IR/LinalgTypes.h"
#include "mlir/Pass/Pass.h"
#include "mlir/StandardOps/Ops.h"
#include "mlir/Stencil/StencilOps.h"
#include "mlir/Support/Functional.h"
#include "mlir/Support/LLVM.h"
#include "mlir/Support/STLExtras.h"
#include "mlir/Transforms/DialectConversion.h"

using namespace mlir;

namespace {

Operation *createContextOperation(StringRef name, Type type, Location loc,
                                  PatternRewriter &rewriter) {
  StringAttr attr = StringAttr::get(name, rewriter.getContext());
  return rewriter.create<Stencil::ContextOp>(loc, type, attr);
}

class VerticalRegionOpConversion : public ConversionPattern {
public:
  VerticalRegionOpConversion(MLIRContext *context)
      : ConversionPattern(Stencil::VerticalRegionOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult matchAndRewrite(Operation *op, ArrayRef<Value *> operands,
                                     BlockAndValueMapping &mapping,
                                     PatternRewriter &rewriter) const override {
    Type indexType = rewriter.getIndexType();
    Location opLoc = op->getLoc();
    Operation *iStartOp =
        createContextOperation("istart", indexType, opLoc, rewriter);
    Operation *iEndOp =
        createContextOperation("iend", indexType, opLoc, rewriter);
    Operation *jStartOp =
        createContextOperation("jstart", indexType, opLoc, rewriter);
    Operation *jEndOp =
        createContextOperation("jend", indexType, opLoc, rewriter);

    ArrayAttr arrayAttr = op->getAttrOfType<ArrayAttr>("extents");
    if (!arrayAttr)
      return matchFailure();
    ArrayRef<Attribute> extents = arrayAttr.getValue();

    int64_t iMinusExtent = extents[0].dyn_cast<IntegerAttr>().getInt();
    int64_t iPlusExtent = extents[1].dyn_cast<IntegerAttr>().getInt();
    int64_t jMinusExtent = extents[2].dyn_cast<IntegerAttr>().getInt();
    int64_t jPlusExtent = extents[3].dyn_cast<IntegerAttr>().getInt();

    MLIRContext *context = op->getContext();
    AffineExpr dimExpr = getAffineDimExpr(0, context);

    AffineMap iMinusMap = AffineMap::get(
        1, 0, dimExpr + getAffineConstantExpr(iMinusExtent, context));
    AffineMap iPlusMap = AffineMap::get(
        1, 0, dimExpr + getAffineConstantExpr(iPlusExtent + 1, context));
    AffineMap jMinusMap = AffineMap::get(
        1, 0, dimExpr + getAffineConstantExpr(jMinusExtent, context));
    AffineMap jPlusMap = AffineMap::get(
        1, 0, dimExpr + getAffineConstantExpr(jPlusExtent + 1, context));
    AffineMap kPlusMap =
        AffineMap::get(1, 0, dimExpr + getAffineConstantExpr(1, context));

    const AffineMap identityMap = AffineMap::getMultiDimIdentityMap(1, context);

    auto kLoop = rewriter.create<AffineForOp>(opLoc, operands[0], identityMap,
                                              operands[1], kPlusMap);
    auto insertionPoint = rewriter.saveInsertionPoint();
    rewriter.setInsertionPointToStart(&kLoop.getRegion().front());

    if (op->getAttr("backward")) {
      auto map = AffineMap::get(1, 2,
                                getAffineSymbolExpr(0, op->getContext()) +
                                    getAffineSymbolExpr(1, op->getContext()) -
                                    getAffineDimExpr(0, op->getContext()));
      auto kVar = rewriter
                      .create<AffineApplyOp>(
                          opLoc, map,
                          std::array<Value *, 3>{kLoop.getInductionVar(),
                                                 operands[0], operands[1]})
                      .getResult();
      rewriter.create<Stencil::InductionVarOp>(
          opLoc, StringAttr::get("K", op->getContext()), kVar);
    } else {
      rewriter.create<Stencil::InductionVarOp>(
          opLoc, StringAttr::get("K", op->getContext()),
          kLoop.getInductionVar());
    }

    AffineForOp iLoop =
        rewriter.create<AffineForOp>(opLoc, iStartOp->getResult(0), iMinusMap,
                                     iEndOp->getResult(0), iPlusMap);
    rewriter.setInsertionPointToStart(&iLoop.getRegion().front());
    rewriter.create<Stencil::InductionVarOp>(
        opLoc, StringAttr::get("I", op->getContext()), iLoop.getInductionVar());

    AffineForOp jLoop =
        rewriter.create<AffineForOp>(opLoc, jStartOp->getResult(0), jMinusMap,
                                     jEndOp->getResult(0), jPlusMap);

    jLoop.getRegion().getBlocks().clear();
    jLoop.getRegion().takeBody(op->getRegion(0));
    jLoop.getRegion().front().addArgument(rewriter.getIndexType());
    rewriter.setInsertionPointToStart(&jLoop.getRegion().front());
    rewriter.create<Stencil::InductionVarOp>(
        opLoc, StringAttr::get("J", op->getContext()), jLoop.getInductionVar());
    jLoop.getRegion().front().getOperations().pop_back();
    rewriter.setInsertionPointToEnd(&jLoop.getRegion().front());
    rewriter.create<AffineTerminatorOp>(op->getLoc());

    rewriter.restoreInsertionPoint(insertionPoint);
    rewriter.replaceOp(op, llvm::None);
    return matchSuccess();
  }
};

class AffineTypeConverter : public TypeConverter {
public:
  explicit AffineTypeConverter(MLIRContext *context) : context(context) {}

  MLIRContext *getContext() { return context; }

private:
  MLIRContext *context;
};

void populateStencilToAffineConversionPatterns(
    AffineTypeConverter &converter, OwningRewritePatternList &patterns) {
  RewriteListBuilder<VerticalRegionOpConversion>::build(patterns,
                                                        converter.getContext());
}

class StencilVerticalRegionToLoopsPass
    : public ModulePass<StencilVerticalRegionToLoopsPass> {
public:
  void runOnModule() override {
    Module m = getModule();

    for (Function f : m) {
      for (Block &b : f) {
        int64_t iMinusExtent = 0, iPlusExtent = 0;
        int64_t jMinusExtent = 0, jPlusExtent = 0;
        for (auto op = b.rbegin(); op != b.rend(); ++op) {
          if (isa<Stencil::VerticalRegionOp>(*op)) {
            Type intType = IntegerType::get(32, (*op).getContext());
            SmallVector<Attribute, 4> extents{
                IntegerAttr::get(intType, iMinusExtent),
                IntegerAttr::get(intType, iPlusExtent),
                IntegerAttr::get(intType, jMinusExtent),
                IntegerAttr::get(intType, jPlusExtent)};
            (*op).setAttr("extents",
                          ArrayAttr::get(extents, (*op).getContext()));
            Block &verticalRegionBody = (*op).getRegion(0).front();
            for (auto vop = verticalRegionBody.rbegin();
                 vop != verticalRegionBody.rend(); ++vop) {
              if (isa<Stencil::ReadOp>(*vop)) {
                Stencil::ReadOp readOp = cast<Stencil::ReadOp>(*vop);
                Value *offsetOperand = readOp.getOperand(1);
                Operation *offsetOp = offsetOperand->getDefiningOp();
                if (Stencil::ConstantOffsetOp constantOffsetOp =
                        dyn_cast<Stencil::ConstantOffsetOp>(offsetOp)) {
                  int64_t iOffset = constantOffsetOp.i().getLimitedValue();
                  int64_t jOffset = constantOffsetOp.j().getLimitedValue();
                  iMinusExtent = std::min(iMinusExtent, iOffset);
                  iPlusExtent = std::max(iPlusExtent, iOffset);
                  jMinusExtent = std::min(jMinusExtent, jOffset);
                  jPlusExtent = std::max(jPlusExtent, jOffset);
                }
              }
            }
          }
        }
      }
    }

    AffineTypeConverter affineConverter(&getContext());
    OwningRewritePatternList affinePatterns;
    populateStencilToAffineConversionPatterns(affineConverter, affinePatterns);

    ConversionTarget stencilAffineTarget(getContext());
    stencilAffineTarget
        .addLegalOp<AffineForOp, AffineApplyOp, AffineTerminatorOp>();
    stencilAffineTarget.addLegalOp<
        Stencil::AddOp, Stencil::SubOp, Stencil::MulOp, Stencil::DivOp,
        Stencil::MinOp, Stencil::MaxOp, Stencil::AndOp, Stencil::OrOp,
        Stencil::EqualOp, Stencil::NotEqualOp, Stencil::LtOp, Stencil::GtOp,
        Stencil::LeOp, Stencil::GeOp, Stencil::NegOp, Stencil::ReadOp,
        Stencil::WriteOp, Stencil::TempOp, Stencil::ContextOp,
        Stencil::GetGlobalOp, Stencil::SetGlobalOp, Stencil::DeclareGlobalOp,
        Stencil::InductionVarOp, Stencil::IfOp>();
    stencilAffineTarget.addLegalOp<ReturnOp>();
    if (failed(applyConversionPatterns(m, stencilAffineTarget, affineConverter,
                                       std::move(affinePatterns))))
      signalPassFailure();
  }
};

static PassRegistration<StencilVerticalRegionToLoopsPass>
    pass("stencil-vertical-region-to-loops",
         "Convert stencil vertical regions to affine for loops");

} // namespace
