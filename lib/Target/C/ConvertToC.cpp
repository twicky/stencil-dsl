//===- ConvertToLLVMIR.cpp - MLIR to LLVM IR conversion ---------*- C++ -*-===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements a translation between the MLIR C dialect and C.
//
//===----------------------------------------------------------------------===//

#include "mlir/Target/C.h"
#include "mlir/C/COps.h"
#include "mlir/C/CTypes.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Module.h"
#include "mlir/StandardOps/Ops.h"
#include "mlir/Stencil/StencilOps.h"
#include "mlir/Translation.h"

#include "mlir/Support/FileUtilities.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Support/raw_ostream.h"

#include <map>
#include <sstream>

using namespace mlir;

namespace {
// Implementation class for module translation.  Holds a reference to the module
// being translated, and the mappings between the original and the translated
// functions, basic blocks and values.
class ModuleTranslation {
public:
  // Translate the given MLIR module expressed in MLIR C dialect into a
  // C string.
  static std::string translateModule(Module &m);

private:
  ModuleTranslation(MLIRContext &c)
      : Context(c), StreamPtr(new llvm::raw_string_ostream(Output)),
        Stream(*StreamPtr) {}

  void convert(Module &module);
  void convert(Function &function);
  void convert(Type &t, Location loc);
  void convert(Block &b, bool ignoreReturn = false);
  void convert(Operation &operation);
  void convert(ReturnOp &returnOp);
  void convert(C::ForOp &forOp);
  void convert(CallOp &callOp);
  void convert(ConstantOp &constantOp);
  void convert(Stencil::DeclareGlobalOp &declareGlobalOp);
  void convert(Stencil::ConstantOffsetOp &constantOffsetOp);
  void convert(C::MaxIOp &maxIOp);
  void convert(C::MaxFOp &maxFOp);
  void convert(C::MinIOp &minIOp);
  void convert(C::MinFOp &minFOp);
  void convert(Stencil::IfOp &ifOp);

  // Register an mlir value and obtain a corresponding C function name.
  std::string getName(Value *v);

  // Explicitly allocate a name for a given mlir value. This name will be
  // used when the given value is referenced via getName().
  void setName(Value *v, std::string name);

  // Explicitly free the name registered for a given mlir value. This is
  // typically used if a value has gone out of scope, but might later be
  // reused (e.g., loop induction variables).
  void freeName(Value *v);

  // Rest all reserved names. This function is expected to be called at the
  // beginning of a function to ensure we start again with small variable
  // identifiers.
  void resetNames();

  // Print indentation according to the current indentation level.
  void printIndent();

  // Increase indentation level.
  void increaseIndent();

  // Reduce indentation level.
  void reduceIndent();

  // The current MLIR context;
  MLIRContext &Context;

  // A map from mlir values to its corresconding C variable name.
  std::map<Value *, std::string> NameMap;

  // The next unused name identifier.
  int NextNameId = 0;

  // The current indentation level.
  int Indent = 0;

  std::string Output;
  llvm::raw_string_ostream *StreamPtr;
  llvm::raw_ostream &Stream;
};

} // end anonymous namespace

std::string mlir::translateModuleToC(Module &m) {
  return ModuleTranslation::translateModule(m);
}

std::string ModuleTranslation::translateModule(Module &M) {
  ModuleTranslation Translator(*M.getContext());

  Translator.convert(M);

  return Translator.StreamPtr->str();
}

static std::string generateBoundaryAccessor(std::string boundName) {
  return std::string("int32_t ") + boundName +
         "start() {\n"
         "return m_dom." +
         boundName +
         "minus();\n"
         "}\n"
         "int32_t " +
         boundName +
         "end() {\n"
         "return m_dom." +
         boundName + "size() == 0 ? 0 : m_dom." + boundName +
         "size() - m_dom." + boundName +
         "plus() - 1;\n"
         "}\n";
}

void ModuleTranslation::convert(Module &m) {
  Function initGlobals = m.getNamedFunction("init_globals");
  std::ostringstream setters;
  std::ostringstream getters;
  std::ostringstream globalsStruct;
  if (initGlobals) {
    globalsStruct << "struct globals {\n";
    for (Block &b : initGlobals) {
      for (Operation &op : b) {
        if (isa<Stencil::DeclareGlobalOp>(op)) {
          StringAttr nameAttr = op.getAttrOfType<StringAttr>("name");
          TypeAttr typeAttr = op.getAttrOfType<TypeAttr>("type");
          Type type = typeAttr.getValue();
          std::string name = nameAttr.getValue().str();
          std::string typeStr;
          if (type.isInteger(1))
            typeStr = "bool";
          else if (type.isInteger(32) || type.isInteger(64))
            typeStr = "int";
          else if (type.isF32())
            typeStr = "float";
          else if (type.isF64())
            typeStr = "double";
          else
            emitError(UnknownLoc::get(m.getContext()),
                      "Unsupported global variable type");

          globalsStruct << typeStr << " " << name << ";\n";

          getters << typeStr << " get_" << name << "() { return m_globals."
                  << name << "; }\n\n";
          setters << "void set_" << name << "(" << typeStr << " " << name
                  << ") { m_globals." << name << " = " << name << "; }\n\n";
        }
      }
    }
    globalsStruct << "};\n";
  }

  Stream << "#include <list>\n"
            "#include <cmath>\n"
            "#include <utility>\n"
            "#include <cassert>\n"
            "#include <cstdint>\n"
            "#if defined(__arm__) || defined(_M_ARM)\n"
            "using half = _Float16;\n"
            "#endif\n\n"
            "#define GRIDTOOLS_CLANG_GENERATED 1\n"
            "#define GRIDTOOLS_CLANG_BACKEND_T CXXNAIVE\n"
            "#ifndef BOOST_RESULT_OF_USE_TR1\n"
            "#define BOOST_RESULT_OF_USE_TR1 1\n"
            "#endif\n"
            "#ifndef BOOST_NO_CXX11_DECLTYPE\n"
            "#define BOOST_NO_CXX11_DECLTYPE 1\n"
            "#endif\n"
            "#ifndef GRIDTOOLS_CLANG_HALO_EXTEND\n"
            "#define GRIDTOOLS_CLANG_HALO_EXTEND 3\n"
            "#endif\n"
            "#ifndef BOOST_PP_VARIADICS\n"
            "#define BOOST_PP_VARIADICS 1\n"
            "#endif\n"
            "#ifndef BOOST_FUSION_DONT_USE_PREPROCESSED_FILES\n"
            "#define BOOST_FUSION_DONT_USE_PREPROCESSED_FILES 1\n"
            "#endif\n"
            "#ifndef BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS\n"
            "#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS 1\n"
            "#endif\n"
            "#ifndef GT_VECTOR_LIMIT_SIZE\n"
            "#define GT_VECTOR_LIMIT_SIZE 30\n"
            "#endif\n"
            "#ifndef BOOST_FUSION_INVOKE_MAX_ARITY\n"
            "#define BOOST_FUSION_INVOKE_MAX_ARITY GT_VECTOR_LIMIT_SIZE\n"
            "#endif\n"
            "#ifndef FUSION_MAX_VECTOR_SIZE\n"
            "#define FUSION_MAX_VECTOR_SIZE GT_VECTOR_LIMIT_SIZE\n"
            "#endif\n"
            "#ifndef FUSION_MAX_MAP_SIZE\n"
            "#define FUSION_MAX_MAP_SIZE GT_VECTOR_LIMIT_SIZE\n"
            "#endif\n"
            "#ifndef BOOST_MPL_LIMIT_VECTOR_SIZE\n"
            "#define BOOST_MPL_LIMIT_VECTOR_SIZE GT_VECTOR_LIMIT_SIZE\n"
            "#endif\n"
            "\n"
            "#include \"gridtools/clang_dsl.hpp\"\n"
            "using namespace gridtools::clang;\n"
            "namespace mlir_stencil {\n\n";
  Stream << globalsStruct.str() << "\n";

  for (Function stencilFunction : m.getFunctions()) {
    if (stencilFunction.getName() == "init_globals" ||
        stencilFunction.isExternal())
      continue;
    SmallVector<std::string, 2> storageTypes;
    unsigned int i = 0;
    for (auto &argument : stencilFunction.getArguments())
      storageTypes.push_back(std::string("StorageType") + std::to_string(i++));
    std::string stencilName =
        std::string("stencil_") + std::to_string(rand() % 8192);
    Stream << "class " << stencilFunction.getName()
           << "{\n"
              "private:\n"
              "struct sbase {\n"
              "virtual void run() {}\n"
              "virtual ~sbase() {}\n"
              "};\n"
              "template <";
    for (i = 0; i < storageTypes.size() - 1; ++i) {
      Stream << "class " << storageTypes[i] << ", ";
    }
    Stream << "class " << storageTypes[i]
           << ">\n"
              "struct "
           << stencilName
           << " : public sbase {\n"
              "using tmp_halo_t = gridtools::halo<GRIDTOOLS_CLANG_HALO_EXTEND, "
              "GRIDTOOLS_CLANG_HALO_EXTEND, 0>;\n"
              "using tmp_meta_data_t = storage_traits_t::storage_info_t<0, 3, "
              "tmp_halo_t>;\n"
              "using tmp_storage_t = "
              "storage_traits_t::data_store_t<float_type, tmp_meta_data_t>;\n"
              "std::list<tmp_storage_t> m_temps;\n"
              "std::list<gridtools::data_view<tmp_storage_t>> m_tempViews;\n"
              "const gridtools::clang::domain& m_dom;\n";
    if (initGlobals)
      Stream << "const globals& m_globals;\n";
    i = 0;
    for (auto &argument : stencilFunction.getArguments()) {
      Stream << storageTypes[i++] << "& m_arg" << argument->getArgNumber()
             << ";\n";
    }
    Stream << "\npublic:\n"
           << stencilName << "(const gridtools::clang::domain& dom_,";
    if (initGlobals)
      Stream << "const globals& globals_,";
    for (i = 0; i < stencilFunction.getNumArguments() - 1; ++i) {
      Stream << storageTypes[i] << "& m_arg" << i << "_, ";
    }
    Stream << storageTypes[i] << "& m_arg" << i
           << "_) :\n"
              "m_temps{}, m_dom(dom_)";
    if (initGlobals)
      Stream << ", m_globals(globals_)";
    for (i = 0; i < stencilFunction.getNumArguments(); ++i) {
      Stream << ", m_arg" << i << "(m_arg" << i << "_)";
    }
    Stream << "{}\n\n"
              "~"
           << stencilName
           << "() {}\n\n"
              "void sync_storages() {\n";
    for (i = 0; i < stencilFunction.getNumArguments(); ++i) {
      Stream << "m_arg" << i << ".sync();\n";
    }
    Stream << "}\n\n";
    Stream << getters.str()
           << "virtual void run() {\n"
              "sync_storages();\n"
              "{\n";
    for (i = 0; i < stencilFunction.getNumArguments(); ++i) {
      Stream << "gridtools::data_view<" << storageTypes[i] << "> view" << i
             << " = gridtools::make_host_view(m_arg" << i
             << ");\n"
                "std::pair<int, void*> arg"
             << i << " = std::make_pair(" << i
             << ", reinterpret_cast<void*>(&view" << i
             << "));\n"
                "std::array<int, 3> arg"
             << i << "_offsets{0, 0, 0};\n";
    }
    Stream << stencilFunction.getName() << "(";
    for (i = 0; i < stencilFunction.getNumArguments() - 1; ++i) {
      Stream << "reinterpret_cast<void*>(&arg" << i << "), ";
    }
    Stream << "reinterpret_cast<void*>(&arg" << i << "));\n}\n";
    Stream << "sync_storages();\n"
              "}\n\n";
    convert(stencilFunction);
    Stream << generateBoundaryAccessor("i") << generateBoundaryAccessor("j")
           << generateBoundaryAccessor("k")
           << "double readTemp(void* view, int32_t i, int32_t j, int32_t k, "
              "void* offset) {\n"
              "return "
              "(*(reinterpret_cast<gridtools::data_view<tmp_storage_t>*>(view))"
              ")(i + (reinterpret_cast<int32_t*>(offset))[0], j + "
              "(reinterpret_cast<int32_t*>(offset)[1]), k + "
              "(reinterpret_cast<int32_t*>(offset))[2]);\n"
              "}\n"
              "void writeTemp(void* view, double value, int32_t i, int32_t j, "
              "int32_t k) {\n"
              "(*(reinterpret_cast<gridtools::data_view<tmp_storage_t>*>(view))"
              ")(i, j, k) = value;\n"
              "}\n"
              "double read(void* view, int32_t i, int32_t j, int32_t k, void* "
              "offset) {\n"
              "std::pair<int, void*> p = *(reinterpret_cast<std::pair<int, "
              "void*>*>(view));\n"
              "if(p.first == 0) {\n"
              "return "
              "(*(reinterpret_cast<gridtools::data_view<StorageType0>*>(p."
              "second)))(i + (reinterpret_cast<int32_t*>(offset))[0], j + "
              "(reinterpret_cast<int32_t*>(offset))[1], k + "
              "(reinterpret_cast<int32_t*>(offset))[2]);\n"
              "}\n";
    for (i = 1; i < stencilFunction.getNumArguments(); ++i) {
      Stream << "else if(p.first == " << i
             << ") {\n"
                "return (*(reinterpret_cast<gridtools::data_view<StorageType"
             << i
             << ">*>(p.second)))(i + (reinterpret_cast<int32_t*>(offset))[0], "
                "j + (reinterpret_cast<int32_t*>(offset))[1], k + "
                "(reinterpret_cast<int32_t*>(offset))[2]);\n"
                "}\n";
    }
    Stream << "else { assert(false && \"unreachable\"); }\n"
              "}\n"
              "void write(void* view, double value, int32_t i, int32_t j, "
              "int32_t k) {\n"
              "std::pair<int, void*> p = *(reinterpret_cast<std::pair<int, "
              "void*>*>(view));\n"
              "if(p.first == 0) {\n"
              "(*(reinterpret_cast<gridtools::data_view<StorageType0>*>(p."
              "second)))(i, j, k) = value;"
              "}\n";
    for (i = 1; i < stencilFunction.getNumArguments(); ++i) {
      Stream << "else if(p.first == " << i
             << ") {\n"
                "(*(reinterpret_cast<gridtools::data_view<StorageType"
             << i
             << ">*>(p.second)))(i, j, k) = value;\n"
                "}\n";
    }
    Stream
        << "}\n"
           "void* temp() {\n"
           "m_temps.push_back(tmp_storage_t(tmp_meta_data_t(m_dom.isize(), "
           "m_dom.jsize(), m_dom.ksize())));\n"
           "m_tempViews.push_back(gridtools::make_host_view(m_temps.back()));\n"
           "return reinterpret_cast<void*>(&m_tempViews.back());"
           "}\n"
           "};\n"
           "static constexpr const char* s_name = \""
        << stencilFunction.getName() << "\";\n";
    if (initGlobals)
      Stream << "globals m_globals;\n";
    Stream << "sbase* m_" << stencilName
           << ";\n\n"
              "public:\n"
           << stencilFunction.getName() << "(const "
           << stencilFunction.getName() << "&) = delete;\n\n"
           << stencilFunction.getName()
           << "(const gridtools::clang::domain& dom";
    for (i = 0; i < stencilFunction.getNumArguments(); ++i) {
      Stream << ", storage_ijk_t& arg" << i;
    }
    Stream << ") : m_" << stencilName << "(new " << stencilName << "<";
    for (i = 0; i < stencilFunction.getNumArguments() - 1; ++i) {
      Stream << "storage_ijk_t, ";
    }
    Stream << "storage_ijk_t>(dom";
    if (initGlobals)
      Stream << ", m_globals";
    for (i = 0; i < stencilFunction.getNumArguments(); ++i) {
      Stream << ", arg" << i;
    }
    Stream << ")) {\n";
    if (initGlobals)
      Stream << "init_globals();\n";
    Stream << "}\n\n";
    if (initGlobals)
      convert(initGlobals);
    Stream << setters.str() << getters.str()
           << "void run() {\n"
              "m_"
           << stencilName
           << "->run();\n"
              "}\n"
              "};\n";
  }
  Stream << "} // namespace mlir_stencil\n\n";
}

void ModuleTranslation::convert(Type &type, Location loc) {
  if (FloatType floatType = type.dyn_cast<FloatType>()) {
    switch (floatType.getWidth()) {
    case 16:
      Stream << "half";
      break;
    case 32:
      Stream << "float";
      break;
    case 64:
      Stream << "double";
      break;
    default:
      emitError(loc, "Bitwidth of floating point type not 16/32/64");
    }
    return;
  }
  if (IntegerType integerType = type.dyn_cast<IntegerType>()) {
    switch (integerType.getWidth()) {
    case 1:
      Stream << "bool";
      break;
    case 8:
      Stream << "int8_t";
      break;
    case 16:
      Stream << "int16_t";
      break;
    case 32:
      Stream << "int32_t";
      break;
    case 64:
      Stream << "int64_t";
      break;
    default:
      emitError(loc, "Bitwidth of integer type not 1/8/16/32/64");
    }
    return;
  }
  if (IndexType indexType = type.dyn_cast<IndexType>()) {
    Stream << "int32_t";
    return;
  }

  if (C::VoidPtrType voidPtrType = type.dyn_cast<C::VoidPtrType>()) {
    Stream << voidPtrType.getCRepr();
    return;
  }

  emitError(loc, "Unsupported type");
}

void ModuleTranslation::convert(Function &f) {
  NameMap.clear();

  FunctionType funcTy = f.getType();

  if (funcTy.getNumResults() == 0)
    Stream << "void";
  else if (funcTy.getNumResults() == 1) {
    Type retTy = f.getType().getResult(0);
    convert(retTy, f.getLoc());
  } else {
    emitError(f.getLoc(), "Functions with two or more results not supported\n");
  }

  Stream << " ";

  Stream << f.getName();

  Stream << "(";
  for (unsigned i = 0; i < f.getType().getNumInputs(); i++) {
    if (i != 0)
      Stream << ", ";

    Type ArgTy = f.getType().getInput(i);
    convert(ArgTy, f.getLoc());

    Stream << " ";

    std::string ArgName = "v_" + std::to_string(i);
    Stream << ArgName;
  }
  Stream << ")";

  if (f.begin() == f.end()) {
    Stream << ";\n";
    Stream << "\n";
    return;
  }

  resetNames();

  for (unsigned i = 0; i < f.getType().getNumInputs(); i++)
    getName(f.getArgument(i));

  Stream << " {\n";

  Block &B = f.front();

  if ((++f.begin()) != f.end()) {
    emitError(f.getLoc(), "Functions with more than one block not supported");
    return;
  }

  increaseIndent();
  convert(B);
  reduceIndent();

  Stream << "}\n";

  Stream << "\n";
}

void ModuleTranslation::convert(Block &B, bool IgnoreReturn) {
  for (Operation &Op : B) {

    if (IgnoreReturn && B.getTerminator() == &Op)
      break;

    convert(Op);
  }
}

void ModuleTranslation::convert(Stencil::DeclareGlobalOp &op) { return; }

void ModuleTranslation::convert(Stencil::ConstantOffsetOp &op) {
  Stream << "{" << op.i() << ", " << op.j() << ", " << op.k() << "};\n";
}

void ModuleTranslation::convert(C::ForOp &forOp) {
  static int loopDepth = 0;

  Region &region = forOp.getBody();

  if ((++region.begin()) != region.end()) {
    emitError(forOp.getLoc(),
              "For bodies with more than one block not supported");
    return;
  }

  Block &bb = region.front();
  Value *iteratorValue = forOp.getIterator();

  std::string lowerBound = getName(forOp.getLowerBound());
  std::string upperBound = getName(forOp.getUpperBound());
  std::string iterator = "i_" + std::to_string(loopDepth);
  setName(iteratorValue, iterator);

  Stream << "for (int32_t " << iterator << " = " << lowerBound << "; "
         << iterator << " < " << upperBound << "; " << iterator << "++) {\n";

  loopDepth++;

  increaseIndent();

  convert(bb, true);

  reduceIndent();
  freeName(iteratorValue);

  printIndent();
  Stream << "}\n";

  loopDepth--;
  return;
}

void ModuleTranslation::convert(Stencil::IfOp &ifOp) {
  static int nestDepth = 0;

  Region &thenRegion = ifOp.thenRegion();

  if ((++thenRegion.begin()) != thenRegion.end()) {
    emitError(ifOp.getLoc(),
              "'then' region with more than one block not supported");
    return;
  }

  Block &bb = thenRegion.front();
  std::string condition = getName(ifOp.getOperand());

  Stream << "if (" << condition << ") {\n";

  nestDepth++;

  increaseIndent();

  convert(bb, true);

  reduceIndent();

  printIndent();
  Stream << "}";

  if (!ifOp.elseRegion().empty()) {
    Region &elseRegion = ifOp.elseRegion();

    if ((++elseRegion.begin()) != elseRegion.end()) {
      emitError(ifOp.getLoc(),
                "'else' region with more than one block not supported");
      return;
    }

    Block &block = elseRegion.front();

    Stream << " else {\n";

    nestDepth++;

    increaseIndent();

    convert(block, true);

    reduceIndent();

    printIndent();
    Stream << "}";
  }

  Stream << "\n";

  nestDepth--;
  return;
}

void ModuleTranslation::convert(ReturnOp &retOp) {
  Stream << "return";

  if (retOp.getNumOperands() == 1)
    Stream << " " << getName(retOp.getOperand(0));

  Stream << ";\n";
}

void ModuleTranslation::convert(CallOp &callOp) {
  Stream << callOp.getCallee().str();

  Stream << "(";

  for (unsigned i = 0; i < callOp.getNumOperands(); i++) {
    if (i != 0)
      Stream << ", ";

    Stream << getName(callOp.getOperand(i));
  }

  Stream << ");\n";
  return;
}

void ModuleTranslation::convert(ConstantOp &constantOp) {
  Attribute attr = constantOp.getValue();

  if (auto intAttr = attr.dyn_cast<IntegerAttr>())
    Stream << intAttr.getValue();
  if (auto floatAttr = attr.dyn_cast<FloatAttr>()) {
    Stream << floatAttr.getValueAsDouble();
    if (constantOp.getType().isF32())
      Stream << "f";
  }
  Stream << ";\n";
}

void ModuleTranslation::convert(C::MaxIOp &maxIOp) {
  Stream << "std::max(" << getName(maxIOp.getOperand(0)) << ", "
         << getName(maxIOp.getOperand(1)) << ");";
}

void ModuleTranslation::convert(C::MinIOp &minIOp) {
  Stream << "std::min(" << getName(minIOp.getOperand(0)) << ", "
         << getName(minIOp.getOperand(1)) << ");";
}

void ModuleTranslation::convert(C::MaxFOp &maxFOp) {
  Stream << "std::max(" << getName(maxFOp.getOperand(0)) << ", "
         << getName(maxFOp.getOperand(1)) << ");";
}

void ModuleTranslation::convert(C::MinFOp &minFOp) {
  Stream << "std::min(" << getName(minFOp.getOperand(0)) << ", "
         << getName(minFOp.getOperand(1)) << ");";
}

void ModuleTranslation::convert(Operation &op) {
  if (isa<Stencil::InductionVarOp>(op))
    return;

  printIndent();

  if (op.getNumResults() == 1) {
    Value *Result = op.getResult(0);
    Type ResType = Result->getType();

    if (!isa<Stencil::ConstantOffsetOp>(op)) {
      convert(ResType, op.getLoc());
      Stream << " ";
      Stream << getName(Result) << " = ";
    } else if (isa<Stencil::ConstantOffsetOp>(op)) {
      Stream << "int32_t " << getName(Result) << "[] = ";
    }
  }

  if (auto forOp = dyn_cast<C::ForOp>(op)) {
    convert(forOp);
    return;
  }

  if (auto retOp = dyn_cast<ReturnOp>(op)) {
    convert(retOp);
    return;
  }

  if (auto callOp = dyn_cast<CallOp>(op)) {
    convert(callOp);
    return;
  }

  if (auto constantOp = dyn_cast<ConstantOp>(op)) {
    convert(constantOp);
    return;
  }

  if (auto declareGlobalOp = dyn_cast<Stencil::DeclareGlobalOp>(op)) {
    convert(declareGlobalOp);
    return;
  }

  if (auto constantOffsetOp = dyn_cast<Stencil::ConstantOffsetOp>(op)) {
    convert(constantOffsetOp);
    return;
  }

  if (auto maxOp = dyn_cast<C::MaxIOp>(op)) {
    convert(maxOp);
    return;
  }

  if (auto minOp = dyn_cast<C::MinIOp>(op)) {
    convert(minOp);
    return;
  }

  if (auto maxOp = dyn_cast<C::MaxFOp>(op)) {
    convert(maxOp);
    return;
  }

  if (auto minOp = dyn_cast<C::MinFOp>(op)) {
    convert(minOp);
    return;
  }

  if (auto ifOp = dyn_cast<Stencil::IfOp>(op)) {
    convert(ifOp);
    return;
  }

  std::string opName;

  if (isa<C::AddIOp>(op) || isa<C::AddFOp>(op))
    opName = "+";
  else if (isa<C::SubIOp>(op) || isa<C::SubFOp>(op))
    opName = "-";
  else if (isa<C::MulIOp>(op) || isa<C::MulFOp>(op))
    opName = "*";
  else if (isa<C::DivFOp>(op))
    opName = "/";
  else if (isa<C::AndOp>(op))
    opName = "&&";
  else if (isa<C::OrOp>(op))
    opName = "||";
  else if (isa<C::EqualOp>(op))
    opName = "==";
  else if (isa<C::NotEqualOp>(op))
    opName = "!=";
  else if (isa<C::LtOp>(op))
    opName = "<";
  else if (isa<C::GtOp>(op))
    opName = ">";
  else if (isa<C::LeOp>(op))
    opName = "<=";
  else if (isa<C::GeOp>(op))
    opName = ">=";

  if (opName != "") {
    Stream << getName(op.getOperand(0));
    Stream << " " << opName << " ";
    Stream << getName(op.getOperand(1));

    Stream << ";\n";
  } else {
    emitError(op.getLoc(), "Unsupported operation");
  }
}

std::string ModuleTranslation::getName(Value *v) {
  if (NameMap.count(v))
    return NameMap[v];

  std::string name = "v_" + std::to_string(NextNameId);
  NextNameId++;

  NameMap[v] = name;

  return name;
}

void ModuleTranslation::setName(Value *v, std::string name) {
  NameMap[v] = name;
}

void ModuleTranslation::freeName(Value *v) { NameMap.erase(v); }

void ModuleTranslation::resetNames() {
  NameMap.clear();
  NextNameId = 0;
}

void ModuleTranslation::printIndent() {
  for (int i = 0; i < Indent; i++)
    Stream << "  ";
}

void ModuleTranslation::increaseIndent() { Indent++; }

void ModuleTranslation::reduceIndent() { Indent--; }

static TranslateFromMLIRRegistration
    registration("mlir-to-c", [](Module module, StringRef outputFilename) {
      if (!module)
        return failure();

      std::string CString = ModuleTranslation::translateModule(module);
      if (CString == "")
        return failure();

      auto file = openOutputFile(outputFilename);
      if (!file)
        return failure();

      file->os() << CString;
      file->keep();
      return success();
    });
